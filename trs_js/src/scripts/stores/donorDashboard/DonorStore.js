import PatronActionCreator from "factory/PatronActionCreator";
import {PatronEvents} from "actions/Events"
import Cache from "stores/Cache"
let _stagingConsumers=Symbol("stagingConsumers")
export class DonorStore {
  constructor() {
    this.donationCache=new Cache();
    this.historyCache=new Cache();
    this.capacityCache=new Cache();
    this.membershipHistoryCache = new Cache();
    this.consumerWorkspaceReducer=this.consumerWorkspaceReducer.bind(this);
  }
  consumerWorkspaceReducer(state,action) {
    if(action.type===PatronEvents.STAGING_LOADED) {
      this[_stagingConsumers] = action.data || {};
    }
    if(action.type===PatronEvents.DONOR_STATUS_UPDATED) {
      this[_stagingConsumers]=this[_stagingConsumers].map((patron)=>{
        if(patron.donorDashboardId===action.data.patronDonorDashboardId) {
          return Object.assign({},patron,{updatedDonorStatus: action.data.donorStatus});
        }
        return patron;
      })
    }
    return {donorInfo:this}
  }
  get stagingPatrons() {
    if(!this[_stagingConsumers]) {
      PatronActionCreator.loadWorkspace();
      return null;
    }
    return this[_stagingConsumers];
  }
  getDonorDonations(patronId) {
    return this.donationCache.getKey(patronId,PatronActionCreator.loadPatronDonations.bind(PatronActionCreator));
  }
  getDonorMembershipHistory(patronId) {
    return this.membershipHistoryCache.getKey(patronId,PatronActionCreator.loadPatronMembershipHistory.bind(PatronActionCreator));
  }
  getDonorHistory(patronId) {
    return this.historyCache.getKey(patronId,PatronActionCreator.loadPatronHistory.bind(PatronActionCreator));
  }
  getDonorCapacity(patronId) {
    return this.capacityCache.getKey(patronId,PatronActionCreator.loadPatronCapacity.bind(PatronActionCreator));
  }
}
let store=new DonorStore();
export default store;
export let workspaceConsumerReducer=store.consumerWorkspaceReducer;
