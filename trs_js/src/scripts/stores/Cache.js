import {GeneralStates} from "actions/Events"
let _cache=Symbol("data")
export default class Cache {
  constructor() {
    this[_cache] = {};
  }
  getKey(id,action,...args) {
    return new Promise((resolve,reject)=>{
      let itemState=this[_cache][id];
      if(!itemState || itemState.state===GeneralStates.NOT_LOADED) {
          if(action) {
            action.apply(this,[id,...args]).then((data)=>{
              this[_cache][id]={
                data,
                state:GeneralStates.DONE
              }
              resolve(data);
            },(...errorDetails)=>{
              this[_cache][id]={
                data:null,
                errorDetails,
                state:GeneralStates.LOAD_ERROR
              }
              reject.apply(this,errorDetails)
            });
          } else {
            resolve(null);
          }
      } else if(itemState.state===GeneralStates.DONE){
        resolve(itemState.data);
      } else if(itemState.state===GeneralStates.LOAD_ERROR){
        reject.apply(this,itemState.errorDetails)
      }
    })
  }
}
