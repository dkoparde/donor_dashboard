let _otContext = Symbol("otContext");
export class OTContextStore  {
  constructor(data) {
    this[_otContext]=data;
  }
  initialize(data) {
    this[_otContext]=data;
    return this;
  }
  getClientContext(clientId) {
	 return this[_otContext].clientContext;
  }

  get clientContext() {
	 return this.getClientContext();
  }

  get consumerContext() {
	 return this[_otContext].consumerContext
  }

  get donationTitle() {
			var preferences = this[_otContext].clientContext.preferences;
			return (preferences && preferences.donations && preferences.donations.displayName!==null)?preferences.donations.displayName : "";
  }

  get productTitle() {
			var preferences = this[_otContext].clientContext.preferences;
			return (preferences && preferences.products && preferences.products.displayName!==null)?preferences.products.displayName: "";
  }

  get packageTitle() {
			var preferences = this[_otContext].clientContext.preferences;
			return (preferences && preferences.packages && preferences.packages.displayName!==null)?preferences.packages.displayName : "" ;
  }

  get clientPreferences() {
      return this[_otContext].clientContext.preferences;
  }
  get currencySymbol() {
      return this[_otContext].clientContext.currencySymbol;
  }
  get clientCurrency() {
      return this[_otContext].clientContext.clientCurrency;
  }
}

export default new OTContextStore();
