export const SmoothScroll = function(position) {
  let TIME = 500; //1 second
  let FRAME_RATE=40; //25fps
  return new Promise((resolve,reject)=> {
    let frames=TIME/FRAME_RATE;
    let msPerFrame=1000/FRAME_RATE;
    let pixelsPerFrame=(position-document.body.scrollTop)/frames;
    let loopId=null
    let progress=()=>{
      window.scrollTo(0,document.body.scrollTop+pixelsPerFrame);
      frames--;
      if(frames<=0) {
        window.scrollTo(0,position);
        resolve()
      } else {
        setTimeout(progress,msPerFrame)
      }
    };
    setTimeout(progress,msPerFrame)
  })
}
