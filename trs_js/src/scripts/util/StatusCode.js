export const SuccessCodes = {
	OK : 200,	
	NO_CONTENT : 204,
	MULTI_STATUS : 207	
}

export const ErrorCodes = {
	BAD_REQUEST : 400,	
	NOT_FOUND : 404	
}