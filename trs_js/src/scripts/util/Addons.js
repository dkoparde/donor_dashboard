Array.prototype.mapObject= function(callback) {
  var ret={};
  for(var i=0; i<this.length; i++) {
    var item=callback(this[i],i);
    if(item && typeof item ==="object" && item.key) {
      ret[item.key]=item.value;
    }
  }
  return ret;
}

//from W3 schools example
class Cookie {
 static getItem(cname) {
   var name = cname + "=";
   var ca = document.cookie.split(';');
   for(var i = 0; i <ca.length; i++) {
       var c = ca[i];
       while (c.charAt(0)==' ') {
           c = c.substring(1);
       }
       if (c.indexOf(name) == 0) {
           return c.substring(name.length,c.length);
       }
   }
   return null;
 }
 static setItem(name,value) {
   var d = new Date();
   var exdays=1;
   d.setTime(d.getTime() + (exdays*24*60*60*1000));
   var expires = "expires="+d.toUTCString();
   document.cookie = name + "=" + value + "; " + expires;
 }
 static removeItem(cname) {
   document.cookie=cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
 }
}


let TEST_KEY="__TeSt_KeY!__"
export class LocalPersistenceFactory {
  static getLocalPersistence() {
    if(LocalPersistenceFactory.canUseSessionStorage()) {
      return window.sessionStorage;
    }
    var cookieEnabled=(navigator.cookieEnabled)? true : false;
  	if(!cookieEnabled){
  		return null;
  	}
    return Cookie;
  }
  static getItem(name) {
    let persistence=LocalPersistenceFactory.getLocalPersistence();
    if(persistence) {
      return persistence.getItem(name);
    }
    return null;
  }
  static setItem(name,value) {
    let persistence=LocalPersistenceFactory.getLocalPersistence();
    if(persistence) {
      persistence.setItem(name,value);
    }
  }
  static removeItem(name) {
    let persistence=LocalPersistenceFactory.getLocalPersistence();
    if(persistence) {
      persistence.removeItem(name);
    }
  }
  static canUseSessionStorage() {
    try {
      if(!window.sessionStorage) {return false;}
      sessionStorage.setItem(TEST_KEY,"test");
      sessionStorage.removeItem(TEST_KEY);
      return true;
    } catch(e) {
      return false;
    }
  }
}
