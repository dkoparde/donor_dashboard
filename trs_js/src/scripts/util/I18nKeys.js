export const Checkout = {
  Cart: {
    total:"checkout.cart.total",
    subTotal:"checkout.cart.subTotal",
    salesTax:"checkout.cart.salesTax",
    viewDetails:"checkout.cart.viewDetails",
    hideDetails:"checkout.cart.hideDetails",
    confirmDelete: "checkout.cart.confirmDelete",
    clearCart: "checkout.cart.clearCart",
    Tickets: {
      show:"checkout.cart.tickets.show",
      date:"checkout.cart.tickets.date",
      time:"checkout.cart.tickets.time",
      price: "checkout.cart.tickets.price",
      priceMobile: "checkout.cart.tickets.priceMobile",
      priceLevelName: "checkout.cart.tickets.priceLevelName",
      quantity:"checkout.cart.tickets.quantity",
      quantityMobile: "checkout.cart.tickets.quantityMobile",
      total:"checkout.cart.tickets.total",
      title:"checkout.cart.tickets.title",
      type: "checkout.cart.tickets.type",
      seat: "checkout.cart.tickets.seat",
      section: "checkout.cart.tickets.section",
      sectionMobile: "checkout.cart.tickets.sectionMobile"
    },
    GiftCards: {
      to:"checkout.cart.giftCards.to",
      from:"checkout.cart.giftCards.from",
      amount:"checkout.cart.giftCards.amount",
      giftCard:"checkout.cart.giftCards.giftCard",
      item:"checkout.cart.giftCards.item",
      giftCardFor:"checkout.cart.giftCards.giftCardFor",
      deliveryType:"checkout.cart.giftCards.deliveryType",
      title:"checkout.cart.giftCards.title"
    },
    Donations: {
      title:"checkout.cart.donations.title",
      payment:"checkout.cart.donations.payment",
      campaign:"checkout.cart.donations.campaign",
      recurrence:"checkout.cart.donations.recurrence",
      donationDate: "checkout.cart.donations.donationDate",
      nextDonation:"checkout.cart.donations.nextDonation",
      amount:"checkout.cart.donations.amount",
      noEndDate: "checkout.cart.donations.noEndDate"
    },
    Products: {
      name:"checkout.cart.products.name",
      price:"checkout.cart.products.price",
      quantity:"checkout.cart.products.quantity",
      total:"checkout.cart.products.total",
      title:"checkout.cart.products.title"
    },
    Packages: {
      name:"checkout.cart.packages.name",
      pricePoint:"checkout.cart.packages.pricePoint",
      amount:"checkout.cart.packages.amount",
      title:"checkout.cart.packages.title"
    },
    TicketName: {
      title:"checkout.cart.ticketName.title",
      addName: "checkout.cart.ticketName.addName",
      changeName: "checkout.cart.ticketName.changeName",
      mobileLabel: "checkout.cart.ticketName.mobileLabel",
      label:"checkout.cart.ticketName.label"
    },
    DeliveryMethod: {
      title: "checkout.cart.deliveryMethod.title",
      defaultOption: "checkout.cart.deliveryMethod.defaultOption",
      label: "checkout.cart.deliveryMethod.label",
      mobileButtonLabel: "checkout.cart.deliveryMethod.mobileButtonLabel",
      notSelectedError: "checkout.cart.deliveryMethod.notSelectedError"
    },
    Guests: {
      addGuests:"checkout.cart.guests.addGuests",
      editGuests:"checkout.cart.guests.editGuests",
      label:"checkout.cart.guests.label",
      guestNumber:"checkout.cart.guests.guestNumber",
      addAnotherGuest: "checkout.cart.guests.addAnotherLink",
      saveGuestsLabel: "checkout.cart.guests.saveGuestsLabel",
      validationError:"checkout.cart.guests.validationError"
    }
  },
  Upsell: {
    defaultOption:"checkout.upsell.defaultOption",
    label:"checkout.upsell.label",
    learnMoreAbout:"checkout.upsell.learnMoreAbout",
    amount:"checkout.upsell.amount",
    otherAmount:"checkout.upsell.otherAmount",
    selectLevel:"checkout.upsell.selectLevel",
    oneTime:"checkout.upsell.oneTime",
    recurring:"checkout.upsell.recurring",
    monthly:"checkout.upsell.monthly",
    quarterly:"checkout.upsell.quarterly",
    yearly:"checkout.upsell.yearly",
    endDonations: "checkout.upsell.endDonations",
    donationCount: "checkout.upsell.donationCount",
    addDonation: "checkout.upsell.addDonation",
    noEndDate: "checkout.upsell.noEndDate",
    otherAmountPlaceholder: "checkout.upsell.otherAmountPlaceholder",
    minimumErrorMessage: "checkout.upsell.minimumErrorMessage",
    numberRequiredErrorMessage: "checkout.upsell.numberRequiredErrorMessage",
    selectEndDate: "checkout.upsell.selectEndDate",
    howOften: "checkout.upsell.howOften",
    greaterThanZero: "checkout.upsell.greaterThanZero"
  },
  Navigation: {
    cartStep:"checkout.nav.cartStep",
    contactDetails:"checkout.nav.contactDetails",
    paymentInfo:"checkout.nav.paymentInfo",
    reviewOrder:"checkout.nav.reviewOrder"
  },
  ContactDetails: {
    title: "checkout.contactDetails.title",
    mailingAddrDifferent:"checkout.contactDetails.mailingAddrDifferent",
    formError:"checkout.contactDetails.formError"
  },
  Questions: {
    title: "checkout.questions.title"
  },
  PaymentInfo: {
    title: "checkout.paymentInfo.title",
    noPaymentNeeded: "checkout.paymentInfo.noPaymentNeeded"
  },
  ReviewOrder: {
    title: "checkout.reviewOrder.title",
    addressHeader: "checkout.reviewOrder.addressHeader",
    paymentInfoHeader: "checkout.reviewOrder.paymentInfoHeader",
    additionalCharges: "checkout.reviewOrder.additionalCharges",
    toc:"checkout.reviewOrder.toc",
    tocAgree:"checkout.reviewOrder.tocAgree",
    defaultSignup: "checkout.reviewOrder.defaultSignup",
    optIn:"checkout.reviewOrder.tmMail",
    privacyLink:"checkout.reviewOrder.privacyLink",
    errors:"checkout.reviewOrder.errors",
    completeOrder:"checkout.reviewOrder.completeOrder",
    wosPrivacyPolicyTitle:"checkout.reviewOrder.wosPrivacyPolicyTitle",
    privacyPolicyTitle:"checkout.reviewOrder.privacyPolicyTitle",
    tmEmailOptinTitle:"checkout.reviewOrder.tmEmailOptinTitle",
    privacyPolicy:"checkout.reviewOrder.privacyPolicy",
    refundPolicy:"checkout.reviewOrder.refundPolicy",
    fundraisingPolicy:"checkout.reviewOrder.fundraisingPolicy",
    nameCaption:"checkout.reviewOrder.refundPolicy",
    guestCaption:"checkout.reviewOrder.guestCaption",
    guestButtonLabel:"checkout.reviewOrder.guestButtonLabel",
    completeOrderButton:"checkout.reviewOrder.completeOrderButton",
    "acceptTosError": "checkout.reviewOrder.acceptTosError",
    paidWithMasterPass:"checkout.reviewOrder.paidWithMasterPass"
  },
  OrderComplete: {
    orderConfirmed:"checkout.orderComplete.orderConfirmed",
    orderInfo:"checkout.orderComplete.orderInfo",
    importantInfo:"checkout.orderComplete.importantInfo",
    additionalNotes:"checkout.orderComplete.additionalInfo",
    boHours:"checkout.orderComplete.boHours",
    boPhone:"checkout.orderComplete.boPhone",
    venueInfo:"checkout.orderComplete.venueInfo",
    printTicket:"checkout.orderComplete.printTicket",
    receipt: "checkout.orderComplete.receipt",
    viewReceipt: "checkout.orderComplete.viewReceipt",
    hideReceipt: "checkout.orderComplete.hideReceipt",
    eTickets: "checkout.orderComplete.eTickets",
    shippingAddress: "checkout.orderComplete.shippingAddress",
    payment: "checkout.orderComplete.payment",
    chargeInfo: "checkout.orderComplete.chargeInfo",
    total: "checkout.orderComplete.total",
    Tickets: {
      orderNumber: "checkout.orderComplete.ticket.orderNumber",
      section: "checkout.orderComplete.ticket.section",
      seat: "checkout.orderComplete.ticket.seat",
      type: "checkout.orderComplete.ticket.type",
      price: "checkout.orderComplete.ticket.price"
    }
  },
  Common: {
    title:"checkout.common.title",
    address:"checkout.common.address",
    zipcode:"checkout.common.zipcode",
    city:"checkout.common.city",
    state:"checkout.common.state",
    phone:"checkout.common.phone",
    firstName: "checkout.common.firstName",
    lastName: "checkout.common.lastName",
    email:"checkout.common.email",
    enterResponse: "checkout.common.enterResponse",
    ccName: "checkout.common.ccName",
    ccNumber: "checkout.common.ccNumber",
    cvv: "checkout.common.cvv",
    cvvInfo: "checkout.common.cvvInfo",
    expDate: "checkout.common.expDate",
    orderProcessing:"checkout.common.orderProcessing"
  }
}
export const ProductionCalendar = {
  usePromoCode:"productionCalendar.usePromoCode",
  promotions:"productionCalendar.promotions",
  regularPrice:"productionCalendar.regularPrice",
  ticketsHeader:"productionCalendar.ticketsHeader",
  totalHeader:"productionCalendar.totalHeader",
  dateHeader:"productionCalendar.dateHeader",
  dateSelectHeader:"productionCalendar.dateSelectHeader",
  ticketSelect:"productionCalendar.ticketSelect",
  backToDetails:"productionCalendar.backToDetails",
  addToCart:"productionCalendar.addToCart",
  checkout:"productionCalendar.checkout",
  removePromo:"productionCalendar.removePromo",
  usePromo:"productionCalendar.usePromo",
  applyPromo:"productionCalendar.applyPromo",
  noPerformanceAvailable:"productionCalendar.noPerformanceAvailable",
  seeEvent: "productionCalendar.seeEvent",
  productionNotFound: "productionCalendar.productionNotFound"
}
export const Common = {
  details:"common.details",
  edit: "common.edit",

  Options: {
    other: "common.options.other",
    blank: "common.options.blank",
    optional: "common.options.optional"
  },
  Navigation: {
    packages:"common.nav.packages",
    calendar:"common.nav.calendar",
    donations:"common.nav.donations",
    giftCards:"common.nav.giftCards",
    products:"common.nav.products",
    shoppingCart:"common.nav.shoppingCart",
    login:"common.nav.login",
    shop:"common.nav.shop"
  },
  Buttons: {
    continue: "common.buttons.continue",
    noThanks: "common.buttons.noThanks",
    submit:"common.buttons.submit",
    close: "common.buttons.close",
    cancel: "common.buttons.cancel",
    delete: "common.buttons.delete",
    learnMore: "common.buttons.learnMore",
    swipe:  "common.buttons.swipe"
  }
}
