export default {
      "clientId": 1,
      "infoMessages": [],
      "warnings": [],
      "tickets": [{
        "productionId": 1,
        "productionName": "Sample Show",
        "productionSubTitle": "A sample.",
        "productionType": "GENERAL_ADMISSION",
        "performanceId": 9288,
        "performance" : {
        	"startDate": 1475888400000,
          entryTimes:[]
        },
        "seats": [{
          "sectionId": 524,
          "sectionName": "General Admission",
          "seatId": 38932,
          "seatNumber": "1475",
          "seatRow": "A",
          "ticketTypeId": 1664,
          "ticketTypeName": "Event Ticket",
          "priceLevelId": null,
          "priceLevelName": null,
          "promotionName": null,
          "promotionId": null,
          "promoMessage": null,
          "ticketRuleId": null,
          "fees": {
            "CONVINIENCE": {
              "id": null,
              "name": "Convinience Fee",
              "price": 6.50,
              "type": "CONVINIENCE"
            },
            "FACILITY": {
              "id": 83,
              "name": "FACILITY FEE",
              "price": 2.00,
              "type": "FACILITY"
            }
          },
          "basePrice": 57.00,
          "price": 57.00,
          "total": 63.50,
          "barcodeString": null
        },
        {
          "sectionId": 524,
          "sectionName": "General Admission",
          "seatId": 38442,
          "seatNumber": "1505",
          "seatRow": "A",
          "ticketTypeId": 1664,
          "ticketTypeName": "Event Ticket",
          "priceLevelId": null,
          "priceLevelName": null,
          "promotionName": null,
          "promotionId": null,
          "promoMessage": null,
          "ticketRuleId": null,
          "fees": {
            "CONVINIENCE": {
              "id": null,
              "name": "Convinience Fee",
              "price": 6.50,
              "type": "CONVINIENCE"
            },
            "FACILITY": {
              "id": 83,
              "name": "FACILITY FEE",
              "price": 2.00,
              "type": "FACILITY"
            }
          },
          "basePrice": 57.00,
          "price": 57.00,
          "total": 63.50,
          "barcodeString": null
        }],
        "total": 114.00,
        "convenienceFees": 13.00
      }],
      "giftCards": [],
      "products": [],
      "packages": [],
      "donations": [],
      "deliveryMethod": null,
      "subTotal": 114.00,
      "total": 127.00,
      "salesTax": 0,
      "enabledSalesTax": true,
      "nameOnTicketFirstName": null,
      "nameOnTicketLastName": null,
      "consumerId": null,
      "addresses": {
        "shippingAddress": {
          "id": null,
          "type": null,
          "usageType": 1,
          "city": null,
          "country": null,
          "line1": null,
          "line2": null,
          "state": null,
          "zipcode": null,
          "empty": true
        },
        "billingAddress": {
          "id": null,
          "type": null,
          "usageType": 1,
          "city": null,
          "country": null,
          "line1": null,
          "line2": null,
          "state": null,
          "zipcode": null,
          "empty": true
        },
        "firstName": null,
        "lastName": null,
        "shippingFirstName": null,
        "shippingLastName": null,
        "email": null,
        "phoneNumber": null,
        "title": null,
        "shippingAddressSameAsBilling": null
      },
      "guests": [],
      "timeout": 1799
    };
