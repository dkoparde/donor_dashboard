export default class DOMUtils {
  static get(selector) {
    return document.querySelector(selector);
  }
  static offset(item) {
    if(typeof item === 'string') {
      item=DOMUtils.get(item);
    }
    let ret={top:0,left:0};
    if(!item) {return ret;}
    let left = 0,top=0;
    do {
       if(!isNaN( item.offsetLeft )) {
          left += item.offsetLeft;
       }
       if(!isNaN( item.offsetLeft )) {
          top += item.offsetTop;
       }
    } while( item = item.offsetParent );
   return {top,left};
  }
  static outerHeight(elm,includeMargin) {
    if(typeof elm === 'string') {
      elm=DOMUtils.get(elm);
    }
    var elmHeight, elmMargin;
    if(document.all) {// IE
        elmHeight = parseInt(elm.currentStyle.height,10);
        elmMargin = parseInt(elm.currentStyle.marginTop, 10) + parseInt(elm.currentStyle.marginBottom, 10);
    } else {// Mozilla
        elmHeight = parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('height'),10);
        elmMargin = parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('margin-top')) + parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('margin-bottom'));
    }
    return (elmHeight+(includeMargin?elmMargin:0));
  }
  static getMargin(item) {
    return DOMUtils.outerHeight(item,true)-DOMUtils.outerHeight(item)
  }
}
