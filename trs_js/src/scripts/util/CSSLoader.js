export class CSSLoader {
  static load(url) {
    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = url;
    link.media = 'all';
    var ret=new Promise((resolve,reject)=> {
      link.addEventListener('load',()=>{
        resolve(url);
      });
      link.addEventListener('error',()=>{
        reject(url)
      })
    })

    head.appendChild(link);
    return ret;
  }
  static loadStyle(styles) {
	    var head  = document.getElementsByTagName('head')[0];
	    var style  = document.createElement('style');
	    style.type = 'text/css';
	    if (style.styleSheet){
	      style.styleSheet.cssText = css;
	    } else {
	      style.appendChild(document.createTextNode(css));
	    }
	    head.appendChild(style);
	    return ret;
	  }
}
export class ScriptLoader {
	  static load(url) {
	    var head  = document.getElementsByTagName('head')[0];
	    var link  = document.createElement('script');
	    link.type = 'text/javascript';
	    link.src = url;
	    var ret=new Promise((resolve,reject)=> {
	      link.addEventListener('load',()=>{
	        resolve(url);
	      });
	      link.addEventListener('error',()=>{
	        reject(url)
	      })
	    })

	    head.appendChild(link);
	    return ret;
	  }
	}
