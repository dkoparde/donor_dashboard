import { combineReducers } from 'redux'
import {workspaceConsumerReducer} from "stores/donorDashboard/DonorStore"


const rootReducer = combineReducers({
  workspaceConsumerReducer
})

export default rootReducer;
