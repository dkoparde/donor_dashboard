import { createStore, applyMiddleware  } from 'redux'
import rootReducer from 'reducers/rootReducer'
import thunk from 'redux-thunk';
import BaseActionCreator from 'factory/BaseActionCreator';
export default function configureStore(initialState) {
  return createStore(rootReducer,
		  initialState,
		  applyMiddleware(
				  thunk
	      )
	)
}
export const ReduxStore = configureStore();
BaseActionCreator.setDispatcher(ReduxStore);
