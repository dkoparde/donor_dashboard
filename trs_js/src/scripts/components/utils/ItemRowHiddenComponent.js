import React from "react";

export class ItemRowHiddenComponent extends React.Component {
  constructor(props) {
    super(props);
    this.close = this.close.bind(this);
  }
  close() {
    this.props.onClose();
  }
  render() {
    return (
      <div className="col-xs-18 ot_ci_hiddenSection">
        {this.props.children}
        <a onClick={this.close} className="btn-close close-btn-top-right"><i className="fa fa-times"></i></a>
      </div>
    );
  }
}
