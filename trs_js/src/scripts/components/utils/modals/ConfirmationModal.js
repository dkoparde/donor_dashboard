import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import React from "react";
import I18nPackages from "stores/I18nStore"
import {Common} from "util/I18nKeys"
import {I18nWrapper} from "components/utils/I18nWrapper";
let I18nCommon = I18nPackages.getForPackage("Common");
export class ConfirmationModal extends React.Component {
  constructor(props) {
    super(props);
  }
  // TODO: make a utility Modal class (maybe) that uses properties for # of buttons,
  //  event handlers, title, etc
  render() {
    return (
      <Modal show={this.props.showModal} onHide={this.props.close}>
        <Modal.Header closeButton>
          <Modal.Title>{this.props.title || "Are You Sure?"}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4>{this.props.message}</h4>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.close}><I18nWrapper I18nResolver={I18nCommon} i18nKey={Common.Buttons.cancel} /></Button>
          <Button onClick={this.props.deleteItem}><I18nWrapper I18nResolver={I18nCommon} i18nKey={Common.Buttons.delete} /></Button>
        </Modal.Footer>
      </Modal>
    )
  }
}
