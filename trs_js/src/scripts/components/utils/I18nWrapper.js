import React from "react";

/**
Renders a wrapper that renders the key to dom for debugging purposes
**/
let _keysLoaded=Symbol("keysLoaded")
export class I18nWrapper extends React.Component{
  constructor(props) {
    super(props);
  }
  componentWillReceiveProps(nextProps) {
    let newState = this.updateValue(nextProps);
    this.setState(newState);
  }
  componentWillMount() {
    this.state = this.updateValue(this.props);
  }
  keysLoaded() {
    this.setState(this.updateValue(this.props));
  }
  updateValue(props) {
    var args=[props.i18nKey].concat(props.arguments||[]);
    var value=null
    if(props.formatted) {
      value=props.I18nResolver.getKeyAsArray.apply(this,args)
    } else {
      value=props.I18nResolver.getKey.apply(this,args)
    }
    return {
      key:props.i18nKey,
      value: value
    }
  }
  render() {
    if(this.props.asHtml) {
      return <span data-i18n-key={this.state.key} {...this.props} dangerouslySetInnerHTML={this.state.value}></span>
    }
    return <span data-i18n-key={this.state.key} {...this.props}>{this.state.value}</span>
  }
}
