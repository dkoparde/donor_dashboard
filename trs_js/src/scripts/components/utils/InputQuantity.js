import React from "react";
import ReactDOM from "react-dom"
var _element=Symbol("element");

let CODE_BACKSPACE = 8;
let CODE_0 =48;
let CODE_9 =57;
let CODE_0_NUMPAD =96;
let CODE_9_NUMPAD =105;
let CODE_LEFT_ARROW = 37;
let CODE_RIGHT_ARROW = 39;
let MAX_OPTIONS = 40;

export class InputQuantity extends React.Component {
  constructor(props) {
    super(props);
    this.state=this.updateState(props);
    this.state.value = props.defaultValue ? props.defaultValue : 0;
    this.handleChange = this.handleChange.bind(this);
    this.handleIncrement = this.handleIncrement.bind(this);
    this.handleDecrement = this.handleDecrement.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur  = this.handleBlur.bind(this)
  }
  componentWillReceiveProps(nextProps) {
    var input = ReactDOM.findDOMNode(this);
    if (nextProps.inputValue && nextProps.inputValue !== this.props.inputValue) {
      this.setState({value:nextProps.inputValue})
    }
    if(  nextProps.defaultChecked !== this.props.defaultChecked) {
      input.defaultChecked = nextProps.defaultChecked;
    }
    this.setState(this.updateState(nextProps));
  }
  updateState(itemProps) {
    var props= {};
    var events={};
    for(var k in itemProps) {
      if(k.indexOf("data-event-")===0) {
        events[k.substring("data-event-".length)]=this.props[k];
      } else {
        props[k]=this.props[k];
      }
    }
    return {props:props,events:events};
  }
  handleFocus(e){
    if(!parseInt(e.target.value)) {
      e.target.value = '';
      e.target.focus();
    }
  }
  handleBlur(e){
    if(e.target.value=== ''){
      e.target.value = 0;
      this.setState({value: 0});
    } else if(e.target.value > this.props.maxValue || e.target.value > MAX_OPTIONS) {
      let maxValue = this.props.maxValue < MAX_OPTIONS ? this.props.maxValue : MAX_OPTIONS
      e.target.value = maxValue;
      this.setState({value: maxValue});
    } else if (e.target.value < this.props.minValue) {
      e.target.value = 0;
      this.setState({value: 0});
    }
    this.props.handleBlur({
      qty: parseInt(e.target.value),
      performanceId: this.props.performanceId,
      ttId: this.props.ttId,
      price: this.props.price,
      section: this.props.section,
      ticketRule: this.props.ticketRule
    });
  }
  isNumeric(keyCode) {
    return (keyCode >= CODE_0 && keyCode <= CODE_9) || (keyCode >= CODE_0_NUMPAD && keyCode <= CODE_9_NUMPAD)
  }
  handleKeyUp(e) { //enforces only digits are typed
    var el=e.target
    if (!this.isNumeric(e.keyCode) && e.keyCode !== CODE_BACKSPACE && e.keyCode !== CODE_LEFT_ARROW && e.keyCode !== CODE_RIGHT_ARROW) {
      //allow digit and backspace and arrow presses, but ignore everything else
      e.preventDefault();
      return false;
    }
    return this.processInput(el, e);

  }
  handleKeyDown(e) {
    e.preventDefault();
    return false;
  }
  processInput(el,e) {
    var el=e.target;

    let currentEl = ReactDOM.findDOMNode(this.refs.qtyInput);
    let currentValue = currentEl.value;
    if(e.keyCode !== CODE_BACKSPACE && e.keyCode !== CODE_LEFT_ARROW && e.keyCode !== CODE_RIGHT_ARROW) {
      currentValue += String.fromCharCode(parseInt(e.keyCode || e.charCode));
    } else if (e.keyCode === CODE_BACKSPACE && currentValue) {
      // should I really be hijacking the native keyboard functionality for bcksp?  is there a way to just let it natively backspace?
      currentValue = currentValue.slice(0, -1);
    }
    this.handleChange({target: { value: currentValue }});
  }
  handleChange(e) {
    this.setState({value: e.target.value});
    let identifiers = {...this.state.props}
    if(this.props.onChange) {
      this.props.onChange(e,identifiers);
    }
  }
  handleIncrement() {
    // if the current value is 0 and there is a non-0 minvalue, set value to minvalue
    if(this.props.minValue && this.state.value == 0 && this.props.minValue != (0 || '')) {
      this.setState({value: this.props.minValue});
      let eventObj = {
        target: {
          value: this.props.minValue
        }
      }
      this.handleChange(eventObj);
    } else if((this.props.maxValue && this.state.value < this.props.maxValue && this.state.value < MAX_OPTIONS) || !this.props.maxValue ) {
      this.setState({value: this.state.value++});
      let eventObj = {
        target: {
          value: this.state.value
        }
      }
      this.handleChange(eventObj);
    } else {
      console.log("maximum tickets which can be bought at one time is " + MAX_OPTIONS + ".  Please contact the box office to purchase more tickets than that.")
    }
  }
  handleDecrement() {
    // if currecnt value is the min value and is decremented, set to 0
    if(this.state.value == this.props.minValue && this.state.value != 0) {
      this.setState({value: 0});
      // sometimes state not set to 0 in time so just hard-coding it in eventObj
      let eventObj = {
        target: {
          value: 0
        }
      }
      this.handleChange(eventObj);
    } else if(this.props.minValue != (null || 'undefined') && ((this.state.value > this.props.minValue && this.state.value != 0) || (this.state.value != (this.props.minValue && 0)))) {
      this.setState({value: this.state.value--});
      let eventObj = {
        target: {
          value: this.state.value
        }
      }
      this.handleChange(eventObj);
    }
  }
  render() {
    var props= {
      ...this.state.props,
      value:this.state.value,
			onChange: this.handleChange
    }
    return (
      <div className={this.props.containerClass}>
        <a onClick={this.handleDecrement}>
          <i className="fa fa-chevron-left"></i>
        </a>
        <input onKeyUp={this.handleKeyUp}
               onKeyDown={this.handleKeyDown}
               onFocus={this.handleFocus}
               onBlur={this.handleBlur}
               ref={"qtyInput"}
               {...props} />
        <a onClick={this.handleIncrement}>
          <i className="fa fa-chevron-right"></i>
        </a>
      </div>
    )
  }
};
