import React from "react"
import {FormElement} from "components/utils/form/FormElement"
import {Checkbox,RadioButton} from "components/utils/form/elements/RadioButton"
import {ValidationUtils} from "util/ValidationAddons"
export class RadioGroup extends FormElement{
  constructor(props) {
    super(props);
    this.elements=[];
  }
  componentWillMount() {
    var defaultValue=this.getDefaultValue()
    this.setState({
      value: defaultValue.length?defaultValue[defaultValue.length-1]:null
    })
  }
  getDefaultValue() {
    if(this.props.defaultValue) {
      return [this.props.defaultValue];
    }
    var ret=[];
    var instanceType=this.instanceType;
    var lookForRadioButtonValues=(parent)=> {
      React.Children.forEach(parent.props.children, (child)=> {
              if(child.type && typeof child.type ==='function' && (child.type.prototype instanceof instanceType || child.type === instanceType) && child.props.checked) {
                ret.push(child.props.value)
              }
      });
    }
    lookForRadioButtonValues(this);
    return ret;
  }
  get values() {
    return this.elements
                  .filter((item)=>!!item.value)
                  .map((item)=>item.value)
  }

  get value() {
    return this.values[0] || null;
  }

  get checked() {
    return this.refs["input"].checked;
  }
  get instanceType() {
    return RadioButton
  }
  addElement(obj) {
    if(obj) {
      this.elements.push(obj)
    }
  }
  isValueSelected(value) {
    return value===this.state.value
  }
  onChange(e) {
    this.setState({value:e.target.value},()=>{
      super.onChange(e);
    })
  }
  cloneChildren(parent) {
    if(!parent || !parent.props || !parent.props.children) {
      return [];
    }
    return React.Children.map(parent.props.children, (child)=> {
          if(typeof child === 'string') {
            return child;
          }
          var ref=null
          var children = null;
          var checked=child.props.checked;
          if(child.type && typeof child.type ==='function' && (child.type.prototype instanceof this.instanceType || child.type === this.instanceType)) {
            ref=this.addElement.bind(this);
            children=child.props.children;
            checked=this.isValueSelected(child.props.value);
          } else {
            children=this.cloneChildren(child);
          }
          return React.cloneElement(child, {
              ref:ref,
              children: children,
              parentForm: this.parentForm,
              checked:checked,
              onChange:this.onChange.bind(this),
              group: this
          });
      });
    }
  render() {
    return <div>
      {this.cloneChildren(this)}
    </div>
  }
}

export class CheckboxGroup extends RadioGroup{
  constructor(props) {
    super(props);
  }
  get instanceType() {
    return Checkbox;
  }
  get value() {
    return this.values.length?this.values:null;
  }
  generateChange(clickFunc) {
    return (e)=>{
      if(clickFunc) {
        clickFunc(e);
      }
      var newValue=this.state.value;
      var selectedValue=e.target.value;
      if(e.target.checked) {
        newValue.push(e.target.value);
      } else if(selectedValue){
        var index=newValue.indexOf(selectedValue);
        if(index>=0) {
          newValue.splice(index,1)
        }
      }
      if(this.props.onChange) {
        this.props.onChange(e,newValue);
      }
      if(this.props.onInput) {
        this.props.onInput(e,newValue);
      }
      this.validate();
      this.setState({value:newValue})
    }
  }
  isValueSelected(value) {
    return this.state.value && this.state.value.indexOf(value)>=0
  }
  validateMinSelections() {
    if(!this.props["data-rule-min-selections"]) {
      return true;
    }
    return this.values.length>=this.props["data-rule-min-selections"];
  }
  validateMaxSelections() {
    if(!this.props["data-rule-max-selections"]) {
      return true;
    }
    return this.values.length<=this.props["data-rule-max-selections"];
  }
  validate() {
    if(!this.value) {
      return  super.validate().valid && this.validateMinSelections();
    }
    var valid=this.validateMinSelections() && this.validateMaxSelections();
    if(this.props["data-rule-custom-validate"]) {
      valid=this.props["data-rule-custom-validate"](this.values,this) && valid;
    }
    if(this.props.validateAny) {
      for(var i=0;i<this.values.length;i++) {
        valid=ValidationUtils.validateValue(this.props,this.values[i],["custom-validate"]) || valid;
      }
    } else {
      for(var i=0;i<this.values.length;i++) {
        valid=ValidationUtils.validateValue(this.props,this.values[i],["custom-validate"]) && valid;
      }
    }
    return {
              invalidElementCount: !valid?1:0,
              valid,
              message:null,
              element: this,
              invalidElements: !valid?[this]:[]
            };
  }
  componentWillMount() {
    this.setState({
      value: this.getDefaultValue()
    })
  }
}
