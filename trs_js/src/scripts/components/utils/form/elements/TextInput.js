import React from "react"
import {FormElement} from "components/utils/form/FormElement"
import {ErrorLabel} from "components/utils/form/elements/ErrorLabel"
import "sass/utils/form/elements/_TextInput.scss"
export class TextInput extends FormElement{
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    this.setState({
      value:this.props.defaultValue || this.props.value
    })
  }
  get value() {
    return this.refs["input"].value
  }
  get type() {
    return "text"
  }
  componentWillReceiveProps(nextProps) {
      if (typeof nextProps.value !== 'undefined' && nextProps.value !== this.state.value) {
        this.setState({value:nextProps.value})
      }
  }
  onChange(e) {
    var event=Object.assign({},e)
    this.setState({value:e.target.value},()=>{
      super.onChange(event);
    })
  }
  render() {
    var {value,className,name,onChange,onBlur,...props}=this.props
    return <span className="ot_ci_textContainer">
      <input type={this.type} ref="input" onBlur={this.onBlur.bind(this)} onChange={this.onChange.bind(this)} value={this.state.value} className={`ot_ci_textBox ${this.cssClass}`} name={this.props.name?this.fullName:null} {...props}/>
      <ErrorLabel className={this.props.errorClass} show={!this.state.hideErrorLabel && this.state.showError && !this.state.isValid} text={this.getValidationMessage()}/>
    </span>
  }
}

export class PhoneInput extends TextInput{
  constructor(props) {
    super(props);
  }
  get type() {
    return "tel"
  }
}
export class TextArea extends TextInput{
  constructor(props) {
    super(props);
  }
  render() {
    var {value, className,name,onChange,onBlur,...props}=this.props
    return <div>
      <textarea ref="input" onChange={this.onChange.bind(this)} value={this.state.value} onBlur={this.onBlur.bind(this)} name={this.props.name?this.fullName:null} className={`ot_ci_textArea ${this.cssClass}`} {...props}/>
      <ErrorLabel show={!this.state.hideErrorLabel && this.state.showError && !this.state.isValid} text={this.getValidationMessage()} className={this.props.labelClass}/>
    </div>
  }
}
