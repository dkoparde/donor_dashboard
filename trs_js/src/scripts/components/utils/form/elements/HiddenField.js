import React from "react"
import {FormElement} from "components/utils/form/FormElement"
export class HiddenField extends FormElement{
  constructor(props) {
    super(props);
  }
  get value() {
    return this.props.value
  }
  render() {
    return null;
  }
}
