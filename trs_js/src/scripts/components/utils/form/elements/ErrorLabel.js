import React from 'react';
export const ErrorLabel = (props) => {
    var {show,text,className,passedProps}=props;
    return <span className={`ot_ci_errorLabel ot_ci_error-msg text-danger ${props.className || ""}`}{...passedProps}>{props.show && props.text}</span>
}
