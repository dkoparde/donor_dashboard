import React from "react"
import {FormElement} from "components/utils/form/FormElement"
import {ErrorLabel} from "components/utils/form/elements/ErrorLabel"
export class RadioButton extends FormElement{
  constructor(props) {
    super(props);
  }
  get group() {
    return this.props.group;
  }
  get value() {
    if(this.checked) {
        return this.props.value || true;
    }
    return this.props.falseValue || null;
  }
  get checked() {
    return this.refs["input"].checked;
  }
  render() {
    return <input type="radio" ref="input" className={this.cssClass} {...this.props}/>
  }
}

export class Checkbox extends RadioButton{
  constructor(props) {
    super(props);
  }
  render() {
    var {onChange,onBlur,...props}=this.props
    return <span>
      <input type="checkbox" onChange={this.onChange.bind(this)} onBlur={this.onBlur.bind(this)} ref="input" className={this.cssClass} {...props}/>
      <ErrorLabel show={!this.props.hideErrorLabel && this.state.showError && !this.state.isValid} className={this.props.labelClass} text={this.getValidationMessage()}/>
      </span>
  }
}
