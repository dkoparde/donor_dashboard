import React from "react"
import {FormElement} from "components/utils/form/FormElement"
import {ErrorLabel} from "components/utils/form/elements/ErrorLabel"
export class Select extends FormElement{
  constructor(props) {
    super(props);
  }
  get value() {
    return this.refs["input"].value
  }
  get validateOnChange() {
    return true;
  }
  render() {
    var {className, onChange,onBlur,...props}=this.props
    return <span>
      <select type="text" ref="input" onBlur={this.onBlur.bind(this)} onChange={this.onChange.bind(this)} className={this.cssClass} {...props}>
        {this.props.children}
      </select>
      <ErrorLabel show={!this.props.hideErrorLabel && this.state.showError && !this.state.isValid} text={this.getValidationMessage()} className={this.props.labelClass}/>
    </span>
  }
}
