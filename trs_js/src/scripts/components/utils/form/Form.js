import React from "react"
import {FormElement} from "components/utils/form/FormElement"
import {TextInput} from "components/utils/form/elements/TextInput"

/***
Forms can be nested to form structured json. Beyond that, there is little value. Note that a form is not an HTML form but a section
***/
export class Form extends FormElement {
  static childContextTypes = {
    rootForm:React.PropTypes.object,
    parentForm:React.PropTypes.object
  }

  constructor(props) {
    super(props);
    this.elements={}
    this.uniqueId=0;
    this.submit=this.submit.bind(this);
    this.validate=this.validate.bind(this);
    this.fullValidate=this.fullValidate.bind(this);
  }
  onValidationError(validationResults) {
    if(this.props.onSubmitValidationError) {
      this.props.onSubmitValidationError(validationResults)
    }
    if(this.props.onValidationError) {
      this.props.onValidationError(validationResults)
    }
  }
  componentDidMount() {
     this.validate();
  }
  clearValidation() {
    Object.keys(this.elements).forEach((key) => {
      this.elements[key].clearValidation();
    });
  }
  submit(options) {
    options=options || {showErrors:true}
    var valid=this.fullValidate(options);
    if(!valid.valid) {
      this.onValidationError(valid);
      return;
    }
    if(this.props.onBeforeSubmit) {
      var ret=this.props.onBeforeSubmit(this.getFormValues());
      if(ret===false) return;
    }
    if(this.props.submit) {
      this.props.submit(this.getFormValues())
    } else if(this.props.onSubmit) {
      this.props.onSubmit(this.getFormValues())
    }
  }
  get value() {
    return this.getFormValues();
  }
  fullValidate(options) {
    options = options || {}
    options.noNotify = true;
    return this.validate(options);
  }
  notifyElementValidationState(name,validObject) {
    var validationResults=this.state.validationResults;
    validationResults[name]=validObject;
    var valid=true;
    Object.keys(validationResults).forEach((el)=> {
      valid=valid && validationResults[el].valid;
    });
    if(this.parentForm) {
      this.parentForm.notifyElementValidationState(this.name,{message:validationResults,element:this,valid:valid})
    }
    if(valid!==this.state.valid && this.props.validStateChanged) {
      this.props.validStateChanged(valid,this,null,validationResults)
    }
    this.setState({validationResults,valid})
  }
  validate(options) {
    options=options || {}
    options.noNotify = true;
    options.showErrors = options.showErrors || false;
    var valid=true;
    var errors={};
    var invalidElements=[];
    var invalidElementCount=0;
    Object.keys(this.elements).forEach((key) => {
      let element= this.elements[key];
      if(!element.props.name) {
        return;
      }
      var elementValidationResult=options.showErrors?element.fullValidate(options):element.validate(options);
      invalidElementCount+=elementValidationResult.invalidElementCount;
      if(!elementValidationResult.valid) {
        invalidElements=invalidElements.concat(elementValidationResult.invalidElements)
      }
      valid=elementValidationResult.valid && valid;
      errors[element.name]=elementValidationResult
    })
    if(valid!==this.state.valid && this.props.validStateChanged) {
      this.props.validStateChanged(valid,this,null,errors)
    }
    var state={valid,message:errors,element:this,invalidElementCount,invalidElements};
    if(!valid && options.showErrors && this.props.onHighlight) {
      this.props.onHighlight(errors)
    }
    this.setState({valid,validationResults:errors});
    return state;
  }
  getFormValues() {
    var ret={};
    Object.keys(this.elements).forEach((key) => {
      let value=this.elements[key];
      if(!value.name || value.skipSubmit()) {
        return;
      }
      if(value.allowMultiple) {
        ret[value.name]=ret[value.name] && Array.isArray(ret[value.name]) ?
                            ret[value.name].concat(value.values)
                            : value.values;
      } else {
        ret[value.name]=value.value;
      }
    })
    return ret;
  }
  removeElement(element) {
    if(this.elements[element.formId]) {
      if(this.state.validationResults) {
        delete this.state.validationResults[element.name];
      }
      var success= delete this.elements[element.formId];
      this.validate();
      return success;
    }
    return false;
  }
  addElement(obj) {
    if(obj) {
      var uniqueId=this.uniqueId++;
      this.elements[uniqueId]=obj;
      this.validate();
      return uniqueId;
    }
    return null;
  }

  getChildContext() {
    return {
      rootForm: this.context.rootForm || this,
      parentForm:this
    }
  }
  render() {
    var content=<section data-type="form" style={this.props.style} className={this.props.className} ref="form">
      {this.props.children}
    </section>
    if(!this.context.rootForm) {
      return <form>{content}</form>
    }
    return content;
  }
}
