import React from "react";
import {Button} from "components/utils/form/buttons/Button"
import {FormElement} from "components/utils/form/FormElement"
export class ButtonGroup extends FormElement {
  constructor(props) {
    super(props);
    this.state={};
  }
  get value() {
    return this.state.value || null;
  }
  componentWillMount() {
    var selectedIndex=null;
    var value=null;
    React.Children.map(this.props.children,(child,index)=>{
      if((!this.props.defaultValue && child.props.active) || (this.props.defaultValue && child.props.value===this.props.defaultValue)) {
        selectedIndex=index;
        value=child.props.value;
      }
    });
    this.setState({selectedIndex:selectedIndex,value:value});
  }
  selectButton(child,childIndex,event) {

    this.setState({
      selectedIndex:childIndex,
      value:event.target.value
    })
    if(child.props.onClick) {
      child.props.onClick(event);
    }
    if(this.props.onChange) {
      child.props.onChange(event.target.value);
    }
  }
  render() {
    var activeButton = (child,index)=> {
        var active=this.state.selectedIndex>=0?this.state.selectedIndex===index:child.props.active;
        var classes=`${child.props.className || ''}`
        return React.cloneElement(child,{active:active,className:classes,selectedIndex:this.state.selectedIndex, onClick:this.selectButton.bind(this,child,index)});
    }
    var {className,onChange,...props}=this.props;
    var clonedChildren=React.Children.map(this.props.children,activeButton);
    return <div className={`btn-group ${className || ''}`} {...props}>
        {clonedChildren}
    </div>
  }
}
ButtonGroup.propTypes = {
    children: function (props, propName, componentName) {
      var error;
      var prop = props[propName];

      React.Children.forEach(prop, function (child) {
        if (!child.type || (child.type instanceof Button)) {
          error = new Error(  '`' + componentName + '` only accepts children of type `Button`.');
        }
      });

      return error;
    }
}

export class ButtonGroup2 extends FormElement {
  static childContextTypes = {
    parentGroup:React.PropTypes.object
  }
  constructor(props) {
    super(props);
    this.btnUniqueId=0;
    this.elements={}
    this.state={value:props.value};
  }
  //when the dust settles try to figure out the default active state
  componentDidMount() {
    if(typeof this.props.defaultValue !=undefined) {
      this.setValue(this.props.defaultValue);
    } else if(typeof this.props.value !=undefined) {
      this.setValue(this.props.value);
    }
  }
  componentWillReceiveProps(nextProps) {
      if (typeof nextProps.value !== 'undefined' && nextProps.value !== this.state.value) {
        this.setValue(nextProps.value)
      }
  }
  setValue(value) {
    let _this=this;
    Object.keys(this.elements).forEach((key)=>{
      _this.elements[key].toggleActive(_this.elements[key].value === value);
    })
    this.setState({value});
  }
  getChildContext() {
    return {
      parentGroup:this
    }
  }
  removeElement(id) {
    if(this.elements[id]) {
      return delete this.elements[id];
    }
    return false;
  }
  addElement(obj) {
    if(obj) {
      var uniqueId=this.btnUniqueId++;
      this.elements[uniqueId]=obj;
      return uniqueId;
    }
    return null;
  }
  get value() {
    return this.state.value || null;
  }

  selectButton(event,child,childId) {
    this.setState({
      selectedIndex:childId,
      value:child.value
    },()=>{
      Object.keys(this.elements).forEach((key)=>{
        this.elements[key].toggleActive(this.elements[key].btnUniqueId === childId)
      })
      if(this.props.onChange) {
        this.props.onChange(child.value,this);
      }
    })
  }
  render() {
    return <div {...this.props}>{this.props.children}</div>
  }
}
