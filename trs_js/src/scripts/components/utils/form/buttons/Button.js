import React from "react";
import {FormElement} from "components/utils/form/FormElement"
import "sass/components/common/buttons/_buttons.scss";
export class Button extends FormElement {
  constructor(props,type) {
    super(props);
    type=(typeof type === 'string'?type:null)
    this.state={
      buttonType:props.buttonType || type  || ''
    }
    this.click=this.click.bind(this);
    this.componentDidMount=this.__componentDidMount;
    this.componentWillUnmount=this.__componentWillUnmount;
  }
  __componentDidMount() {
    super._componentDidMount.apply(this,arguments)
    if(this.context.parentGroup) {
      this.btnUniqueId=this.context.parentGroup.addElement(this);
    }
  }
  __componentWillUnmount() {
    super._componentWillUnmount.apply(this,arguments)
    if(this.context.parentGroup) {
      this.context.parentGroup.removeElement(this.btnUniqueId);
    }
  }

  toggleActive(active) {
    this.setState({active})
  }
  get value() {
    return this.props.value;
  }
  click(e) {
    var proceed=true
    if(this.context.parentGroup) {
      this.context.parentGroup.selectButton(e,this,this.btnUniqueId)
    }
    if(this.props.onClick) {
      proceed=this.props.onClick(e)
    }
    var submit=this.props.submit || this.props.type==="submit";
    if(proceed!==false && this.rootForm && submit) {
      this.rootForm.submit();
    }
  }
  render() {
    var {className,onClick,type, ...props}=this.props;
    var size='';
    switch(this.props.size) {
      case 'large': size='ot_btn-lg'; break;
      case 'medium': size='ot_btn-md'; break;
      case 'small': size='ot_btn-sm'; break;
    }
    var active=(this.state.active?'active':'');
    var disabledClass=(props.disabled?'disabled':'');
    return <button onClick={this.click} type="button" className={`btn ${this.state.buttonType} ${size} ${active} ${disabledClass} ${this.props.className || ""}`} {...props}>{this.props.children}</button>

  }
}
Button.contextTypes=Object.assign({},FormElement.contextTypes,{
  parentGroup:React.PropTypes.object
});
export class SubmitButton extends Button {
  constructor(props) {
    super(props,'btn-success');
  }
}
export class PrimaryButton extends Button {
  constructor(props) {
    super(props,'btn-primary');
  }
}
export class PrimaryToggle extends Button {
  constructor(props) {
    super(props,'btn-toggle-primary');
  }
}
export class DefaultButton extends Button {
  constructor(props) {
    super(props,'btn-default');
  }
}
export class CancelButton extends Button {
  constructor(props) {
    super(props,'btn-close');
  }
}
export class TransparentButton extends Button {
  constructor(props) {
    super(props,'btn-transparent');
  }
}
export class IconButton extends Button {
  constructor(props) {
    super(props,'btn-icon');
  }
}
export class LinkButton extends Button {
  constructor(props) {
    super(props,'ot_btn-link');
  }
}
