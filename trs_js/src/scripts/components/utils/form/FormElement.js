import React from "react"
import {ValidationUtils} from "util/ValidationAddons"

export class FormElement extends React.Component{
  constructor(props) {
    super(props);
    this.state={isValid:true}
    this._onMount=this.componentDidMount || (()=>{});
    this.componentDidMount=this._componentDidMount;
    this._onBeforeUnmount=this.componentWillUnmount || (()=>{});
    this.componentWillUnmount=this._componentWillUnmount;
  }
  _componentDidMount() {
    this._onMount.apply(this,arguments)
    if(this.parentForm) {
      this.uniqueId=this.parentForm.addElement(this);
    }
  }
  _componentWillUnmount() {
    this._onBeforeUnmount.apply(this,arguments)
    if(this.parentForm) {
      this.parentForm.removeElement(this);
    }
  }
  get formId() {
    return this.uniqueId;
  }
  get allowMultiple() {
    return this.props.allowMultiple;
  }
  get values() {
    return [this.value];
  }
  get value() {
    return null;
  }
  get name() {
    return this.props.name
  }
  get fullName() {
    if(this.parentForm && this.parentForm.props.name) {
      return `${this.parentForm.props.name}.${this.name}`;
    }
    return this.name;
  }
  get parentForm() {
    return this.props.parentForm || (this.context && this.context.parentForm);
  }
  get rootForm() {
    return this.props.parentForm || (this.context && this.context.rootForm);
  }
  get cssClass() {
    var errorClass=!this.state.isValid?this.props.errorClass || "ot_ci_input_error":this.props.validClass || "ot_ci_input_success"
    return `${this.state.showError?errorClass:""} ${this.props.className || ''}`
  }
  get isValid() {
    return this.validate().valid;
  }
  get validateOnChange() {
    return false;
  }
  skipSubmit() {
    if(this.props.skipSubmit) {
      return this.props.skipSubmit(this.value,this);
    }
    return false;
  }
  onChange(e) {
    if(this.props.onChange) {
      this.props.onChange(e,this.value,this)
    }
    if(this.validateOnChange || this.props.validateOnChange || (this.props.rootForm && this.rootForm.props.validateOnChange)) {
      this.fullValidate();
    } else {
      this.validate({stateUpdate:false});
    }
    if(this.props.fullValidateOnChange && this.parentForm) {
      this.parentForm.fullValidate({showErrors:!!this.props.fullValidateShowErrors});
    }
    if(this.parentForm && this.parentForm.props.fullValidateOnChange) {
      this.parentForm.fullValidate({showErrors:!!this.parentForm.props.fullValidateShowErrors});
    }
    if(this.rootForm && (this.props.submitOnChange || this.rootForm.props.submitOnChange)) {
      this.forceUpdate(()=>this.rootForm.submit());
    }
  }
  onBlur(e) {
    if(this.props.onBlur) {
      this.props.onBlur(e,this)
    }
    if(this.props.validateOnBlur!==false || (this.parentForm && this.parentForm.props.validateOnBlur!==false)) {
      this.fullValidate();
    } else {
      this.validate({stateUpdate:false});
    }
    if(this.props.fullValidateOnBlur && this.parentForm) {
      this.parentForm.fullValidate({showErrors:!!this.props.fullValidateShowErrors});
    }
    if(this.parentForm && this.parentForm.props.fullValidateOnBlur) {
      this.parentForm.fullValidate({showErrors:!!this.parentForm.props.fullValidateShowErrors});
    }
    if(this.rootForm && (this.props.submitOnBlur || this.rootForm.props.submitOnBlur)) {
      this.forceUpdate(()=>this.rootForm.submit())
    }
  }
  getMessageForValidation(validation) {
    return this.getValidationMessage(null,validation)
  }
  getValidationMessage(ruleName,validationResults) {
    if(!ruleName) {
      if(!this.state.validationResults) {
        return null;
      }
      var {isAllValid,...errors}=validationResults || this.state.validationResults;
      ruleName=Object.keys(errors).filter((key)=>!errors[key])[0];
    }
    if(typeof this.props["data-msg-"+ruleName] === 'function') {
      return this.props["data-msg-"+ruleName](this.value)
    } else if(typeof this.props["data-msg-"+ruleName] !== 'undefined') {
      return this.props["data-msg-"+ruleName]
    }
    return ValidationUtils.getValidationMessage(ruleName);
  }
  clearValidation() {
    this.setState({showError:false});
  }
  forceError(rule) {
    var validationResults={isAllValid:false}
    if(rule) {
      validationResults[rule]=false;
    }
    this.setState({validationResults,isValid:validationResults.isAllValid})
  }
  forceClean() {
    var validationResults={isAllValid:true}
    this.setState({validationResults,isValid:validationResults.isAllValid})
  }
  fullValidate(options) {
    options=options || {}
    options.noNotify = options.noNotify || false;
    this.setState({showError:true});
    return this.validate(options);
  }
  validate(options) {
    options=options || {}
    options.noNotify = options.noNotify || false;
    var validationResults = ValidationUtils.validateValue(this.props,this.value);
    var ret={
              invalidElementCount: !validationResults.isAllValid?1:0,
              valid:validationResults.isAllValid,
              message:this.getMessageForValidation(validationResults),
              element: this,
              invalidElements: !validationResults.isAllValid?[this]:[]
            }
    if(options.stateUpdate!==false) {
      this.setState({validationResults,isValid:validationResults.isAllValid})
    }
    if(this.props.validStateChanged) {
      this.props.validStateChanged(validationResults.isAllValid,this.name,ret,validationResults)
    }
    if(this.parentForm && !options.noNotify) {
      this.parentForm.notifyElementValidationState(this.name,ret)
    }
    return ret;
  }
}
FormElement.contextTypes={
  rootForm:React.PropTypes.object,
  parentForm:React.PropTypes.object
}
