import React from 'react';

export const LoadingIcon = props =>
<i className={`fa fa-${props.size} fa-spin fa-spinner ${props.className}`} />;

LoadingIcon.propTypes = {
  size: React.PropTypes.string, // 'lg', '2x', '3x', '4x', '5x'
  className: React.PropTypes.string, //   "text-heading... "
};

LoadingIcon.defaultProps = {
  size: '2x'
};

export class LoadingView extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return <span className="ot_ci_loadingSpinner ot_ci_mv-40" {...this.props}>
    <i className={`fa fa-${this.props.size || "2x"} fa-spinner fa-pulse fa-fw text-heading `}></i>
      <span className="h1 text-heading ot_ci_mh-20 ot_ci_mh-30 pulse-in-out">{this.props.text || ""}</span>
      </span>
  }
}

export class LoadingOverlay extends React.Component {
  constructor(props) {
    super(props);
  }
  getDefaultContent() {
    return [
      <i key={1} className={this.props.loadingGlyphClass || "fa fa-spin fa-spinner"}></i>
    ]
  }
  render() {
    var style={};
    if(this.props.minHeight) {
      style.minHeight=this.props.minHeight;
    }
    return <span className="ot_ci_loadingOverlay ot_ci_relative ot_ci_verticalCenter text-center" style={style}>
      {this.props.children || this.getDefaultContent() }
    </span>
  }
}
