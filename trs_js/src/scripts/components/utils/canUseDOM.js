// Used when querying DOM in react
// to prevent function to run when the DOM is still not there
// if (canUseDOM) {}
export const canUseDOM = !!(
  (typeof window !== 'undefined' &&
  window.document && window.document.createElement)
);
