import React from 'react';
export class ItemDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      showDetails:this.props.showDetails || false,
      detailsClass: this.props.showDetails?"ot_ci_open":"ot_ci_close"
    };
    this.toggleDetails=this.toggleDetails.bind(this);
    this.childRender=this.render.bind(this);
    this.render=this.baseRender.bind(this);
  }
  showDetails(show) {
    var showDetails=show;
    this.setState({showDetails:showDetails,
      detailsClass:showDetails?"anime_growY ot_ci_open":"anime_shrinkY ot_ci_close"})
  }
  toggleDetails(onOver) {
    var showDetails=!this.state.showDetails;
    this.setState({showDetails:showDetails,
      detailsClass:showDetails?"anime_growY ot_ci_open":"anime_shrinkY ot_ci_close"})
  }
  baseRender() {
    return this.childRender();
  }
}

export class ModalItemDetails extends ItemDetails {
  constructor(props) {
    super(props);
  }
  baseRender() {
    return <span className="ot_ci_modalItemDetails">
      <div className={"ot_ci_relativeBackdrop fade "+(!this.state.showingDetails?"out":"in")} onClick={this.toggleDetails}></div>
      {super.baseRender()}
  </span>
  }
}
