import {Scrollspy} from "react-scrollspy";
export class OTScrollspy extends Scrollspy {
  constructor(props) {
    super(props);
  }
  _getElemsViewState (targets) {
    const targetItems = targets ? targets : this.state.targetItems
    let viewStates=targetItems.map(this._computeViewState);
    let viewStatusList = Array(targetItems.length).fill(false);
    let lastInView =-1
    for(let i=0; i<viewStates.length;i++) {
      let viewStatus=viewStates[i];
      if(viewStatus.containsView || (viewStatus.wholeElementInView && viewStatus.isAboveMidPoint)) {
        lastInView=i;
        break;
      } else if(viewStatus.isInView && (viewStatus.isAboveMidPoint || i<0)) {
        lastInView = i;
      }
    }
    if(lastInView>=0) {
      viewStatusList[lastInView] =true;
    }
    return {
      inView: targetItems.filter((el,index)=>!!viewStatusList[index]),
      outView: targetItems.filter((el,index)=>!viewStatusList[index]),
      viewStatusList,
    }
  }
  _computeViewState(el) {
    if(!el) {
      return {
        isAboveMidPoint: false,
        isInView: false,
        wholeElementInView: false,
        topInView:false,
        bottomInView:false,
        containsView:false
      }
    }
    let topInView=false,
        bottomInView=false,
        containsView=false;
        const winH = window.innerHeight
        const rect = el.getBoundingClientRect()
        const scrollTop = document.documentElement.scrollTop || document.body.scrollTop
        const scrollBottom = scrollTop + winH
        const elTop = rect.top + scrollTop
        const elBottom = elTop + el.offsetHeight;
        topInView = elTop >= scrollTop && elTop<scrollBottom;
        bottomInView = elBottom > scrollTop && elBottom<scrollBottom;
        containsView = elTop<scrollTop && elBottom > scrollBottom;
        return {
          isAboveMidPoint: elTop < (scrollTop+scrollBottom) / 2,
          isInView: topInView || bottomInView || containsView,
          wholeElementInView: topInView && bottomInView,
          topInView,
          bottomInView,
          containsView
        }
  }
}
