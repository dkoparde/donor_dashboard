import React from "react"
import MotionSlider from "react-motion-slider";

export class SliderCard extends React.Component {

    render( ) {
      let {width, className,style,...props} = this.props
        return <li className={`slide ${ className || "" }`} style={this.props.width ? {width: `${this.props.width}`} : null} {...props}>
            {this.props.children}
        </li>
    }
}

export class Slider extends React.Component {
    componentWillMount( ) {
        this.setState({ currentIndex: 0 })
    }
    onChange( currentIndex, nextIndex ) {
        if ( this.props.beforeSlide ) {
            this.props.beforeSlide( currentIndex[0], nextIndex[0] )
        }
        this.setState({ currentIndex: nextIndex[0] });
    }
    getCurrentIndex() {
    	return this.state.currentIndex;
    }
    next() {
      this.refs["slider"].next();
    }
    prev() {
      this.refs["slider"].prev();
    }
    render( ) {
        let {
            beforeSlide,
            className,
            ...props
        } = this.props;
        let childSize = React.Children.count( this.props.children )
        let lastDisabled=this.state.currentIndex <= 0 && "disabled";
        let slidesToShow = this.props.slidesToShow ? this.props.slidesToShow : 1;
        let nextDisabled = (this.state.currentIndex + slidesToShow - 1 >= (childSize - 1)) && "disabled";
        return (
          <ul className={`ot_sliderController ${ className || "" }`}>
            <li className="ot_ci_slider_controllerBtn leftArrow">
              {!this.props.noArrows &&
                <i className={`fa fa-chevron-circle-left ${lastDisabled}`} onClick={( ) => this.refs.slider.prev( )} />
              }
            </li>
            <MotionSlider ref="slider" beforeSlide={this.onChange.bind( this )} {...props}>
                {this.props.children}
            </MotionSlider>
            <li className="ot_ci_slider_controllerBtn rightArrow">
              {!this.props.noArrows &&  <i className={`fa fa-chevron-circle-right ${nextDisabled}`} onClick={( ) => this.refs.slider.next( )}/>}
            </li>
          </ul>
        );
    }
}
export default Slider;

Slider.propTypes = {
    children: function( props, propName, componentName ) {
        var error;
        var prop = props[propName];

        React.Children.forEach( prop, function( child ) {
            if (!child.type || ( child.type instanceof SliderCard )) {
                error = new Error( '`' + componentName + '` only accepts children of type `SliderCard`.' );
            }
        });

        return error;
    }
}
