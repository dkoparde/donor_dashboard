import React from "react";
import Modal from "react-bootstrap/Modal";
import {CancelButton} from "components/utils/form/buttons/Button";
import {I18nWrapper} from "components/utils/I18nWrapper";
import {Common} from "util/I18nKeys";
import I18nPackages from "stores/I18nStore"

let I18nCommon = I18nPackages.getForPackage("Common");
export class MobileModal extends React.Component {
  constructor(props) {
    super(props);
    this.close = this.close.bind(this);
  }
  close() {
    this.props.onClose();
  }
  render() {
    return (
      <Modal show={this.props.showModal}
             onHide={this.close}
             className="col-xs-24" >
        <Modal.Header></Modal.Header>
        <Modal.Body>
          {this.props.modalContent}
        </Modal.Body>
        <Modal.Footer>
          <CancelButton onClick={this.close} size="medium">
            <I18nWrapper I18nResolver={I18nCommon} i18nKey={Common.Buttons.cancel}/>
          </CancelButton>
        </Modal.Footer>
      </Modal>
    );
  }
}
