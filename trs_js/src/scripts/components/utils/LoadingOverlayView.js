import React from "react"
import "sass/components/utils/_loadingOverlay.scss"
export class LoadingOverlayView extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    let {...props} = this.props;
    return (
    	<div className={`ci_loadingOverlay ${this.props.className || ""}`} {...props}>
    		<div className="ci_iconContainer">
		      <span className="h3 ci_loadingOverlayLabel">{this.props.loadingOverlayLabel || this.props.children}</span>
		      <i className={'fa fa-2x ' + (typeof this.props.loadingGlyphClass === 'string' ? this.props.loadingGlyphClass : "fa-pulse fa-spinner")}></i>
	      </div>
      </div>
    )
  }
}
