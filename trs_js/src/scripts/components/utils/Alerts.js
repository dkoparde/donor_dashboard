import React from "react";
import BaseAction from "actions/BaseAction";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import I18nPackages from "stores/I18nStore";
import {CartEvents, GlobalEvents} from "actions/Events";
import {Common} from "util/I18nKeys"

let I18n = I18nPackages.getForPackage("FormError");
let I18nError = I18nPackages.getForPackage("Error");
let I18nCommon = I18nPackages.getForPackage("Common");

var _notificationEvent=Symbol("notificationEvent");
export class Notifications extends React.Component {
  constructor(props) {
    super(props);
    this.state={elements:[]};
  }
  componentWillReceiveProps(newProps) {
    this.handleNotification(newProps.notification);
  }
  handleNotification(data) {
    if(!data) return;
    var elements=this.state.elements;
    if(elements.indexOf(data) === -1 ) {
      elements.push(data);
    }
    if(!this.currentElement) {
      this.nextNotification();
    } else {
      this.setState({elements:elements});
    }

  }
  nextNotification() {
    var elements=this.state.elements ||[];
    var current=elements.shift();
    this.setState({currentElement:current,elements:elements});
  }
  render() {
    if(!this.state.currentElement) {return null;}
    return (
      <div className="static-modal">
        <Modal.Dialog onHide={this.props.onHide || function(){}}>
         <Modal.Header>
        	 <Modal.Title>{this.state.currentElement.title}</Modal.Title>
         </Modal.Header>
         <Modal.Body>
           {this.state.currentElement.body}
        </Modal.Body>
         <Modal.Footer>
           <Button onClick={this.nextNotification.bind(this)}>{I18nCommon.getKey(Common.Buttons.close)}</Button>
         </Modal.Footer>

       </Modal.Dialog>
      </div>
    )
  }
}
export class Alerts extends React.Component {
  constructor(props) {
    super(props);
    this.state={errorQueue: [], messageQueue: [], isCartExpired: false};
  }
  componentWillReceiveProps(newProps) {
    this.handleErrorCartExpiration(newProps.cartExpireAlert);
    this.handleWarningsAndInfoMessages(newProps.messages)
    this.handleError(newProps.errors);
  }
  handleErrorCartExpiration(data) {
	  if(!data) return;
	  var errorQueue = this.state.errorQueue;
	  errorQueue.push(I18nError.getKey("errors.cart.timeout"));
	  this.setState({errorQueue: errorQueue,isCartExpired:true });
  }
  handleWarningsAndInfoMessages(data) {
	  if(!data) return;
  	var messageQueue = this.state.messageQueue;
  	if(data.infoMessages && data.infoMessages.length) {
  		messageQueue.push(data.infoMessages);
  	}
  	if(data.warnings && data.warnings.length) {
  		data.warnings.forEach(function(warning,index) {
        if(messageQueue.indexOf(warning.message)===-1) {
          messageQueue.push(warning.message);
        }
  		})
  	}
  	this.setState({messageQueue: messageQueue});
  }
  handleError(data) {
    if(!data) return;
    try{
      if(data instanceof Error) {
        this.state.errorQueue.push(I18n.getKey("errors.client.unexpected", data.message))
      } else if(data.xhr && !data.xhr.status) {
        this.state.errorQueue.push(I18n.getKey("errors.client.noNetwork"));
      } else if(data.data){
    	  if(data.data.statusCode == '3031') {//if 'shopping cart empty' error gets from server then manually dispatch 'cart expired' to update state of front end data
    		  CartActionCreator.expireCart(data);
    	  }
        this.state.errorQueue.push(data.data.message || I18n.getKey("errors.client.unexpected", data.data.type));
      } else {
        switch(data.xhr && data.xhr.status) {
          case 404: this.state.errorQueue.push(I18n.getKey("errors.client.notFound",data.url)); break;
        }
      }
    } catch(e) {
      this.state.errorQueue.push("I18nError: "+e.message);
      console.error(e);
    }
    this.setState({errorQueue: this.state.errorQueue, messageQueue: this.state.messageQueue})
  }
  clearMessages() {
    this.setState({errorQueue:[],messageQueue:[],isCartExpired:false})
  }
  render() {
    if(!this.state.errorQueue.length && !this.state.messageQueue.length) {return null;}
    return (
      <div className="static-modal">
        <Modal.Dialog onHide={()=>{}}>
         <Modal.Header>
         {this.state.isCartExpired ?
        	 <Modal.Title>{I18nError.getKey("errors.cart.Expired")}</Modal.Title> :
        	 <Modal.Title>{I18n.getKey("errors.client.dialogTitle",this.state.errorQueue.length + this.state.messageQueue.length)}</Modal.Title>
         }
         </Modal.Header>
         {this.state.errorQueue.map(function(error,index) {
           return <Modal.Body key={index}>
             <span dangerouslySetInnerHTML={{__html:error}}></span>
           </Modal.Body>
         })}
         {this.state.messageQueue.map(function(message,index) {
             return <Modal.Body key={index}>
               <span dangerouslySetInnerHTML={{__html:message}}></span>
             </Modal.Body>
           })}
         <Modal.Footer>
           <Button onClick={this.clearMessages.bind(this)}>{I18nCommon.getKey(Common.Buttons.close)}</Button>
         </Modal.Footer>

       </Modal.Dialog>
      </div>
    )
  }
}
