import React from "react"
import  { connect } from 'react-redux';
import {PatronSelect} from "components/donorDashboard/stagingArea/PatronSelect"
import {PatronCard} from "components/donorDashboard/stagingArea/PatronCard"
import 'sass/donorDashboard/_main.scss';

export class _DonorDashboardMain extends React.Component{
  constructor(props) {
    super(props);
    this.state={count:0}
  }
  render() {
    return <div>
    <div className="patronSelectedCardSection">
     <PatronCard selectedPatron={this.state.selectedPatron} donorData={this.state.donorData} getNextDonor={()=>this.refs.patronSelector.selectNextPatron()} getPreviousDonor={()=>this.refs.patronSelector.selectPreviousPatron()}/>
     </div>
    <PatronSelect ref="patronSelector" patrons={this.props.patrons} onPatronSelected={(patron)=>this.setState({selectedPatron: patron.consumerInfo, donorData: patron})}/>
    </div>
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    patrons: state.workspaceConsumerReducer.donorInfo.stagingPatrons
  }
}

export let DonorDashboardMain = connect(mapStateToProps)(_DonorDashboardMain)
