import React from "react";
import {Slider,SliderCard} from "components/utils/Slider"
import 'sass/donorDashboard/stagingArea/_patronSelect.scss';
import { ViewPager, Frame, Track, View } from 'react-view-pager'
export class PatronSelect extends React.Component{
  renderPatronCards() {
    let currentPatron=(this.state && this.state.selectedPatron) || this.props.patrons[0];
    return this.props.patrons.map((patron,index)=>
      <SliderCard style={{width:"33%"}} key={index} onClick={()=>this.selectPatron(patron,index)} className={`patronCard ${currentPatron===patron.consumerInfo.id && 'selected'}`}>
          <div className="updatedDonorValue">{patron.updatedDonorStatus != null ? patron.updatedDonorStatus.replace(/[_]/g, " ") : ''}</div>{`${patron.consumerInfo.firstName} ${patron.consumerInfo.lastName}`}
      </SliderCard>
    )
  }

  selectPatron(patron,index) {
    this.setState({selectedPatron: patron.consumerInfo.id, selectedIndex:index})
    if(this.props.onPatronSelected) {
      this.props.onPatronSelected(patron);
    }
  }
  selectNextPatron(){
    let index = this.state.selectedIndex || 0;
    if (Object.keys(this.props.patrons).length > index){
      let nextIndex = index+1;
      this.selectPatron(this.props.patrons[nextIndex],nextIndex);
    }
  }
  selectPreviousPatron(){
    let index = this.state.selectedIndex || 0;
    if (index>0){
      let prevIndex = index-1;
      this.selectPatron(this.props.patrons[prevIndex],prevIndex);
    }
  }
  render() {
    if(!this.props.patrons || (Object.keys(this.props.patrons).length === 0 && this.props.patrons.constructor === Object)) {
    return <div className="selectPatronMessage">
            <i className="fa fa-spinner fa-pulse fa-fw"></i>
        </div>
    }
    return <div>
      <Slider>
        {this.renderPatronCards()}
      </Slider>
      </div>
  }
}
