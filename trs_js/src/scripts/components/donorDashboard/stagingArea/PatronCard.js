import React from "react";
import 'sass/donorDashboard/stagingArea/_patronCard.scss';
import {LoadingView} from "components/utils/LoadingView";
import PatronActionCreator from "factory/PatronActionCreator";
import DonorStore from "stores/donorDashboard/DonorStore"
import {NumberFormatter,DateFormatter} from "util/Formatter"
import moment from "moment"
import {PrimaryButton} from "components/utils/form/buttons/Button"
import {Select} from "components/utils/form/elements/Select"
export class PatronCard extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      selectedPatron:props.selectedPatron,
      patronData:null
    }
  }
  componentWillReceiveProps(newProps) {
    if(newProps.selectedPatron !== this.props.selectedPatron ) {
      this.fetchPatronDetails(newProps.selectedPatron);
    }
  }
  fetchPatronDetails(selectedPatron){
    this.setState({selectedPatron,loadingPatron:true,loadingDonations:true,loadingHistory:true,loadingCapacity:true},()=>{
      DonorStore.getDonorDonations(selectedPatron.id).then((donationData)=>{
        this.setState({donationData,loadingDonations:false});
      });
      DonorStore.getDonorHistory(selectedPatron.id).then((historyData)=>{
        this.setState({historyData,loadingHistory:false});
      });
      DonorStore.getDonorMembershipHistory(selectedPatron.id).then((membershipHistoryData)=>
        {
          this.setState({membershipHistoryData, loadingMembershipHistory:false});
        })
    })

  }
  updateDonorStatus(donorStatus) {
    PatronActionCreator.updateDonorStatus(this.props.donorData.donorDashboardId, donorStatus);
    this.getNextDonor();
  }

  getNextDonor(){
    this.props.getNextDonor && this.props.getNextDonor();
  }

  getPreviousDonor(){
    this.props.getPreviousDonor && this.props.getPreviousDonor();
  }

  updatePatronDonorStatus(donorStatus){
    PatronActionCreator.updateDonorStatus(this.props.donorData.donorDashboardId, donorStatus);
  }
  renderDonations() {
    if(this.state.loadingDonations) {
      return <div className="patronCardSection"><LoadingView/></div>;
    }
    let donationData = this.state.donationData;
    let currentYear = new Date().getFullYear();
    if (!(donationData.donationYTDValues[currentYear] >= 0)) {
      currentYear = currentYear - 1;
    }
    return <div className="patronCardSection">
      <div className="patronCardDataCell">
        <div className="cardDataCellRow1">Last Donation:</div>
        <div className="cardDataCellRow2">{!donationData.lastDonationDate ? 'N/A' : `${NumberFormatter.formatCurrency(donationData.lastDonationAmount)}`}</div>
        <div className="cardDataCellRow3">{!donationData.lastDonationDate ? `N/A` : `${DateFormatter.formatDate(donationData.lastDonationDate)}`}</div>
      </div>
      <div className="patronCardDataCell">
        <div className="cardDataCellRow1">YTD Donations:</div>
        <div className="cardDataCellRow2">{NumberFormatter.formatCurrency(donationData.donationYTDValues[currentYear])}</div>
      </div>
      <div className="patronCardDataCell">
        <div className="cardDataCellRow1">Last YTD Donations:</div>
        <div className="cardDataCellRow2">{NumberFormatter.formatCurrency(donationData.donationYTDValues[currentYear - 1])}</div>
      </div>
      <div className="patronCardDataCell">
        <div className="cardDataCellRow1">Total Donated:</div>
        <div className="cardDataCellRow2">{NumberFormatter.formatCurrency(donationData.totalAmount)}</div>
        <div className="cardDataCellRow3">{!donationData.donationCount ? '0' : `${donationData.donationCount}`} Donations</div>
      </div>
    </div>
  }
  renderHistory() {
    if(this.state.loadingHistory) {
      return <div className="patronCardSection"><LoadingView/></div>;
    }
    let historyData = this.state.historyData;
    return <div className="patronCardSection">
      <div className="patronCardDataCell">
        <div className="cardDataCellRow1">First Show:</div>
        <div className="cardDataCellRow2">{!historyData.firstPerformance ? 'N/A' : `${DateFormatter.formatDate(historyData.firstPerformance.perfStart)}`}</div>
        <div className="cardDataCellRow3">{!historyData.firstPerformance ? 'N/A' : `${historyData.firstPerformance.production.name}`}</div>
      </div>
      <div className="patronCardDataCell">
        <div className="cardDataCellRow1">Last Show:</div>
        <div className="cardDataCellRow2">{!historyData.lastPerformance ? 'N/A' : `${DateFormatter.formatDate(historyData.lastPerformance.perfStart)}`}</div>
        <div className="cardDataCellRow3">{!historyData.lastPerformance ? 'N/A' : `${historyData.lastPerformance.production.name}`}</div>
      </div>
      <div className="patronCardDataCell">
        <div className="cardDataCellRow1">Next Show:</div>
        <div className="cardDataCellRow2">{!historyData.nextPerformance ? 'N/A' : `${DateFormatter.formatDate(historyData.nextPerformance.perfStart)}`}</div>
        <div className="cardDataCellRow3">{!historyData.nextPerformance ? 'N/A' : `${historyData.nextPerformance.production.name}`}</div>

      </div>
      <div className="patronCardDataCell">
        <div className="cardDataCellRow1">Number of Shows:</div>
        <div className="cardDataCellRow2">{historyData.totalShowsAttended}</div>
      </div>
    </div>
  }
  renderMembership() {
    if(this.state.loadingMembershipHistory) {
      return <div className="patronCardSection"><LoadingView/></div>;
    }
    let membershipHistoryData = this.state.membershipHistoryData;
    return <div className="patronCardSection">
      <div className="patronCardDataCell">
        <div className="cardDataCellRow1">Current Package:</div>
        <div className="cardDataCellRow2">{!membershipHistoryData || !membershipHistoryData.currentPackage ? `N/A` : `${membershipHistoryData.currentPackage.packageName}`}</div>
        <div className="cardDataCellRow3">{!membershipHistoryData || !membershipHistoryData.currentPackage ? `N/A` : `${DateFormatter.formatDate(membershipHistoryData.firstPackage.purchaseDate)}`}</div>
      </div>
      <div className="patronCardDataCell">
        <div className="cardDataCellRow1">Last Package:</div>
        <div className="cardDataCellRow2">{!membershipHistoryData || !membershipHistoryData.lastPackage ? `N/A` : `${membershipHistoryData.lastPackage.packageName}`}</div>
        <div className="cardDataCellRow3">{!membershipHistoryData || !membershipHistoryData.lastPackage ? `N/A` : `${DateFormatter.formatDate(membershipHistoryData.firstPackage.purchaseDate)}`}</div>
      </div>
      <div className="patronCardDataCell">
        <div className="cardDataCellRow1">First Package:</div>
        <div className="cardDataCellRow2">{!membershipHistoryData || !membershipHistoryData.firstPackage ? `N/A` : `${membershipHistoryData.firstPackage.packageName}`}</div>
        <div className="cardDataCellRow3">{!membershipHistoryData || !membershipHistoryData.firstPackage ? `N/A` : `${DateFormatter.formatDate(membershipHistoryData.firstPackage.purchaseDate)}`}</div>
      </div>
      <div className="patronCardDataCell">
        <div className="cardDataCellRow1">Member For:</div>
        <div className="cardDataCellRow2">{!membershipHistoryData || !membershipHistoryData.numberOfYearsPatronHadMembership ? `N/A` : `${membershipHistoryData.numberOfYearsPatronHadMembership} Year(s)`}</div>
      </div>
    </div>
  }
  renderHeader() {
    return <div className="patronCardHeader">
      <div className="patronCardHeaderCell nameHeaderCell">
        <div className="patronName">{this.state.selectedPatron.firstName} {this.state.selectedPatron.lastName}</div>
        <div className="patronId">Patron ID: {this.state.selectedPatron.id}</div>
        <div/>
      </div>
      <div className="patronCardHeaderCell addressHeaderCell">
        <div className="patronCardHeaderCellRow1">Address:</div>
        <div className="patronCardHeaderCellRow2">{!this.state.selectedPatron.primaryAddress ? 'N/A' : `${this.state.selectedPatron.primaryAddress}`}</div>
      </div>
      <div className="patronCardHeaderCell createDateHeaderCell">
        <div className="patronCardHeaderCellRow1">Patron Since:</div>
        <div className="patronCardHeaderCellRow2">{DateFormatter.formatDate(this.state.selectedPatron.createDate)}</div>
      </div>
      <div className="patronCardHeaderCell donorStatusHeaderCell">
      </div>
    </div>
  }
  render() {
    let selectedPatron=this.state.selectedPatron;
    if(this.state.loadingPatron) {
      <LoadingView/>
    }
    if(!selectedPatron) {
      return (
        <div className="selectPatronMessage">
          <h3>Select a Patron</h3>
        </div>
      );
    }
    return (
      <div>
        <div className="patronSelectedCard">
        {this.renderHeader()}
        {this.renderDonations()}
        {this.renderHistory()}
        {this.renderMembership()}
        </div>
        <div className="buttons">
          <span title="PREVIOUS" className="btn-donorCard btn-donorCard-action" onClick={()=>this.getPreviousDonor()}>« PREV</span>
          <span title="NOT PROSPECT" className="btn-donorCard btn-donorCard-no" onClick={()=>this.updateDonorStatus("NOT_PROSPECT")}>✖</span>
          <span title="PROSPECT" className="btn-donorCard btn-donorCard-yes" onClick={()=>this.updateDonorStatus("DONOR_PROSPECT")}>❤</span>
          <span title="NEXT" className="btn-donorCard btn-donorCard-action" onClick={()=>this.getNextDonor()}>NEXT »</span>
        </div>
      </div>
    );
  }
}
