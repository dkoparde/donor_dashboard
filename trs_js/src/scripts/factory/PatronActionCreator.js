import BaseActionCreator from "factory/BaseActionCreator"
import {PatronEvents} from "actions/Events"
export class PatronActionCreator extends BaseActionCreator{
  loadWorkspace(clientId) {
    return this.get("/workspace").then((consumers)=>{
      this.dispatch({type: PatronEvents.STAGING_LOADED,data:consumers});
      return consumers
    })
  }
  loadDonorDetails(patronId) {
    return this.get(`/PatronDonor(${patronId})`).then((patronData)=>{
      this.dispatch({type: PatronEvents.DONOR_DETAILS_LOADED,data:patronData});
      return patronData;
    })
  }
  loadPatronDonations(patronId) {
    return this.get(`/Consumer(${patronId})/donationHistory`).then((patronData)=>{
      this.dispatch({type: PatronEvents.PATRON_DONATIONS_LOADED,data:patronData});
      return patronData;
    })
  }
  loadPatronHistory(patronId) {
    return this.get(`/Consumer(${patronId})/ticketHistory`).then((patronData)=>{
      this.dispatch({type: PatronEvents.PATRON_HISTORY_LOADED,data:patronData});
      return patronData;
    })
  }
  loadPatronMembershipHistory(patronId){
    return this.get(`/Consumer(${patronId})/membershipHistory`).then((patronData)=>
  {
      this.dispatch({type: PatronEvents.PATRON_MEMBERSHIP_HISTORY_LOADED, data: patronData});
      return patronData;
  })
  }
  loadPatronCapacity(patronId) {
    return this.get(`/Consumer(${patronId})/capacity`).then((patronData)=>{
      this.dispatch({type: PatronEvents.PATRON_CAPACITY_LOADED,data:patronData});
      return patronData;
    })
  }
  updateDonorStatus(patronDonorDashboardId, donorStatus) {
    var data = {
      donorStatus : donorStatus
    };
    return this.patch(`/ConsumerDonor(${patronDonorDashboardId})`, data).then(()=>{
      this.dispatch({type: PatronEvents.DONOR_STATUS_UPDATED, data:{donorStatus,patronDonorDashboardId}})
    });
  }
}
export default new PatronActionCreator();
