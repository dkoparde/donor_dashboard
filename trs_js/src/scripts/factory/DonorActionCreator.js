import BaseActionCreator from "factory/BaseActionCreator"
import {DonorDashboardEvents} from "actions/Events"
export class DonorActionCreator extends BaseActionCreator{
  loadWorkspace() {
    return this.get("/workspace").then((consumers)=>{
      this.dispatch({type: DonorDashboardEvents.STAGING_LOADED,data:consumers});
    })
  }
}
export default new DonorActionCreator();
