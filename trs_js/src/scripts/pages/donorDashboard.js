import React from "react"
import ReactDOM from "react-dom";
import {DonorDashboardMain} from "components/donorDashboard/main"
import { Provider } from 'react-redux';
import {ReduxStore} from 'reducers/configureStore'
import 'pages/indexHtml/index.html';

let component = <Provider store={ReduxStore}>
  <DonorDashboardMain onChange={(str)=>alert(str)}/>
</Provider>
ReactDOM.render(component,document.getElementById('donorDashboard'));
