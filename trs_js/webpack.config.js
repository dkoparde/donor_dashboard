var CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require("html-webpack-plugin")

var argv=require("yargs").argv
var buildType="dev";
var webpack = require('webpack');
var path = require('path');
var glob = require("glob");
var entryPoints =  {
  donorDashboard:['./src/scripts/pages/donorDashboard.js'],
  vendor:['react','react-dom','babel-polyfill', 'moment','accounting','react-burger-menu'],
  core: ['config','factory/BaseActionCreator','util/Formatter',
        'util/DOMUtils', 'util/Addons','util/ValidationAddons','util/Formatter','util/I18nKeys',
        'stores/I18nStore']

};
if(argv.apiOnly) {
  entryPoints= {
    OvationtixAPI: './src/scripts/api/OvationtixAPI.js'
  }
  console.log("Building api only...")
} else if(argv.buildAll){
  entryPoints.OvationtixAPI= './src/scripts/api/OvationtixAPI.js'
  console.log("Building api and main...")
} else {
  console.log("Building main only...")
}
var apiDemoHtml="pages/apiDemo/testApi.html"
var apiDemoEntryHtml="pages/apiDemo/testApi.html"
var devTool=null;
if(argv.prod || argv.production || argv.beta) {
	if(argv.sourceMap) {
		devTool='source-map';
	}
  buildType="prod"
  apiDemoHtml="pages/apiDemo/nyker-desktop-demo.html"
  apiDemoEntryHtml="pages/apiDemo/sleep-no-more-desktop-demo.html"
  console.log("for production")
} else if(argv.nykerLocal) {
  devTool="source-map"
  apiDemoHtml="pages/apiDemo/nyker-desktop-demo.html"
  apiDemoEntryHtml="pages/apiDemo/sleep-no-more-desktop-demo.html"
  console.log("for development")
} else if(argv.staging) {
  devTool='inline-source-map';
  apiDemoHtml="pages/apiDemo/nyker-desktop-demo.html"
  apiDemoEntryHtml="pages/apiDemo/sleep-no-more-desktop-demo.html"
  buildType=argv.staging
  console.log("for staging:"+buildType)
} else {
  devTool="source-map"
  console.log("for development")
}
var config=buildType;
if(argv.nyker) {
  config="nyker";
}
if(argv.staging) {
	config="staging"
}
if(argv.apiFile) {
  apiDemoHtml=argv.apiFile;
}
console.log("Api Html file: "+apiDemoHtml)
console.log("Api Html Entry file: "+apiDemoEntryHtml)
var plugins=[
  //new CopyWebpackPlugin([{ from: 'src/pages' }]),
  new webpack.DefinePlugin({'process.env': {
    NODE_ENV: buildType==="prod"?"'production'":"'"+buildType+"'",
    PRODUCTION: buildType==="prod" || buildType==="production" || argv.beta,
    STAGING: buildType.startsWith("staging"),
    DEV: buildType==="dev",
    BETA: argv.beta || false
  }})
];
if(!argv.apiOnly) {
  plugins=plugins.concat([
    new webpack.optimize.CommonsChunkPlugin({name:"core",minChunks:2, filename:"scripts/ovationtix.core.js",chunks:["donorDashboard","core"]}),
    new webpack.optimize.CommonsChunkPlugin(/* chunkName= */"vendor", /* filename= */"scripts/vendor.bundle.js")
  ]);
}
if(argv.prod || argv.production || argv.beta) {
  plugins.push(new webpack.optimize.UglifyJsPlugin({
    minimize: true,
    compress:{warnings:false},
    output: {comments: false}
  }));
}
plugins.push(new ExtractTextPlugin("styles/admin.css"));
var outDir=__dirname+"/"+(argv.outDir || "build");
    module.exports = {
        entry:entryPoints,
        output: {
          publicPath: "/",
            path: outDir,
            filename: 'scripts/[name].js',
            chunkFilename: 'scripts/[name].js',
            library: argv.apiOnly?"Ovationtix":"[name]",
            libraryTarget: "umd"
        },
        stats: {
          colors: true,
          modules: true,
          reasons: true,
          errorDetails: true
        },
        module: {
          preloaders:[{
            test:  /index_wp\.html/i,
            loader: 'string-replace',
            query: {
              search: 'CLIENT_ID',
              replace: "^%",
              flags:"g"
            }
          }],
            loaders: [
              { test: /\.(ttf|eot|svg|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file?name=fonts/[name].[ext]" },
              { test: /apiDemo/i, loader: "file?name=demo/[name].[ext]" },
              { test: /prodTest/i, loader: "file?name=demo/[name].[ext]" },
              { test: /indexHtml/i, loader: "file?name=index.[ext]" },
              {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader?sourceMap!sass-loader?sourceMap")
              },
                {
                  loader: 'babel-loader',
                    test: /\.js$/,
                   exclude: /(node_modules|bower_components)/,
                   query: {
                      presets: ['es2015', 'react', 'stage-0'],
                      plugins: []
                  }
                 }
            ]
        },
        plugins: plugins,
        devtool: devTool,
        resolve: {
          root: __dirname,
          alias: {
        	  	"ReactRouter$": "react-router",
        	  	"errors": "scripts/errors",
        	  	"scripts":"src/scripts",
              "react-bootstrap": "react-bootstrap/lib",
      			"main":"scripts/main",
      			"routes":"scripts/routes",
      			"root":"scripts/root",
      			"dispatcher": "scripts/dispatcher",
      			"stores":"scripts/stores",
      			"factory":"scripts/factory",
      			"reducers":"scripts/reducers",
      			"mobile":"scripts/pages/mobile",
      			"desktop":"scripts/pages/desktop",
      			"components":"scripts/components",
      			"common":"scripts/components/common",
      			"util":"scripts/util",
      			"actions":"scripts/actions",
      			"config":"config/"+config+"/AppConfig.js",
      			"sass": "src/sass",
      			"pages":"src/pages",
      			"links":"pages/links",
      			"external": "node_modules",
      			"apiDemoHtml": apiDemoHtml,
      			"apiDemoEntryHtml": apiDemoEntryHtml
    	    }
        }
    };
