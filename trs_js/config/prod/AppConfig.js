let splitUrl=window.location.pathname.split("/");
export const EnvironmentInfo = {
  environment: "prod",
  debug: false
}
export const UrlInfo={
  clientId: splitUrl[1]
}
export class ConfigContext {
  static setClientContext(context) {
      HTTPConfig.defaultHeaders.clientId=UrlInfo.clientId=context.clientId;
  }
  static getClientContext() {
    return HTTPConfig.defaultHeaders.clientId;
  }
}
let host="https://admin.ovationtix.com";
if(process.env.BETA) {
    host="https://beta.ovationtix.com"
}
export const HTTPConfig = {
  host: host,
  contextRoot:"/trs/api/rest",
  suffix:".ot",
  port:null,
  documentRoot:"trs",
  defaultHeaders:{
    clientId:splitUrl[1],
    "Content-Type":"application/json"
  }
};
