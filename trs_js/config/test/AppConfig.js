let splitUrl=window.location.pathname.split("/");
export const EnvironmentInfo = {
  environment: "staging",
  debug: false
}
export const UrlInfo={
  clientId: 7
}
export class ConfigContext {
  static setClientContext(context) {
      HTTPConfig.defaultHeaders.clientId=UrlInfo.clientId=context.clientId;
  }
  static getClientContext() {
    return HTTPConfig.defaultHeaders.clientId;
  }
  static get i18nDisabled() {
    return true;
  }
}
export const HTTPConfig = {
  host: "https://staging6.ovationtix.com",
  contextRoot:"/ci/api/rest",
  port:null,
  defaultHeaders:{
    clientId:7,
    "Content-Type":"application/json",
    newCIRequest:true
  }
};
export const Links = {
  CALENDAR: "https://staging6.ovationtix.com/ci/cal/"+splitUrl[1],
  PACKAGES: "https://staging6.ovationtix.com/ci/store/"+splitUrl[1]+"/packages",
  PRODUCTS: "https://staging6.ovationtix.com/ci/store/"+splitUrl[1]+"/products",
  DONATIONS: "https://staging6.ovationtix.com/ci/store/"+splitUrl[1]+"/alldonations",
  GIFT_CARD: "https://staging6.ovationtix.com/ci/giftcard/"+splitUrl[1]
};
