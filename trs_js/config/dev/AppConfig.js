let splitUrl=window.location.pathname.split("/");

export const EnvironmentInfo = {
  environment: "dev",
  debug: true
}
export const UrlInfo={
  clientId: splitUrl[1]
}

export class ConfigContext {
  static setClientContext(context) {
      HTTPConfig.defaultHeaders.clientId=UrlInfo.clientId=context.clientId;
  }
  static getClientContext() {
    return HTTPConfig.defaultHeaders.clientId;
  }
}
export const HTTPConfig = {
  host: "//localhost.ovationtix.com:8080",
  contextRoot:"/trs/api/rest",
  suffix:".ot",
  port:null,
  defaultHeaders:{
    clientId:UrlInfo.clientId,
    "Content-Type":"application/json"
  }
};
