let splitUrl=window.location.pathname.split("/");
export const EnvironmentInfo = {
  environment: "staging",
  debug: false
}
export const UrlInfo={
  clientId: splitUrl[1]
}
export class ConfigContext {
  static setClientContext(context) {
      HTTPConfig.defaultHeaders.clientId=UrlInfo.clientId=context.clientId;
  }
  static getClientContext() {
    return HTTPConfig.defaultHeaders.clientId;
  }
}
var hostDomainName = process.env.NODE_ENV ? process.env.NODE_ENV : "staging6"
export const HTTPConfig = {
  host: "https://"+hostDomainName+".ovationtix.com",
  contextRoot:"/trs/api/rest",
  suffix:".ot",
  port:null,
  defaultHeaders:{
    clientId:splitUrl[1],
    "Content-Type":"application/json"
  }
};
