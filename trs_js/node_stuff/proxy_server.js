var httpProxy = require('http-proxy');
var http= require("http");
var url= require("url");
var argv=require("yargs").argv
var fs=require('fs')
var https=require('https')
var crypto=require('crypto')
var options={
  targetUrl: 'http://localhost.ovationtix.com',
  targetPort: 2222,
  mainHtml:"/index.html",
  testApi:argv.apiUrl?argv.apiUrl:"/demo/nyker-desktop-demo.html",
  testEntryApi:argv.apiUrl?argv.apiUrl:"/demo/sleep-no-more-desktop-demo.html",
  mainDir: __dirname+"/../build",
  port: 9999
}
if(argv.port) {
  options.port=argv.port
}
if(argv.targetPort) {
  options.targetPort=argv.targetPort
}
if(argv.targetUrl) {
  options.targetUrl=argv.targetUrl
}
if(argv.mainFile) {
  options.mainHtml=argv.mainFile
}
options.target=options.targetUrl+":"+options.targetPort

console.log("Starting Server at port: "+options.port);
console.log("Proxy will route to: "+options.target);
console.log("API will route to: "+options.testApi);
console.log("CI will route to: "+options.mainHtml);
var proxy = httpProxy.createProxyServer(options);
var serveStatic = require('serve-static');
console.log(__dirname)
var serve = serveStatic(options.mainDir);
proxy.on('error',function(err,req,res){res.writeHead(502);res.end();});
var handler=function(request, response) {
  var req=request;
  var res=response;
  try {
    console.log("Requested URL: "+request.url)
    serve(req,res, function(err) {
      if(req.url.match(/^\/trs\/api\/.+/)!=null || req.url.match(/^\/trs\/.+/)!=null || req.url.match(/^\/ps\/.+/)!=null) {
        console.log("Proxying to backend... "+req.url)
        proxy.web(req, res);
      }else {
        var oldPath=req.url;
       if(req.url.match(/\/testApi(\?.*)?$/)!=null) {
         req.url=options.testApi;
         console.log("Redirecting to api test...")
         serve(req,res, function(err) {
           res.setHeader('Content-Type', 'text/html');
             res.writeHead(res.statusCode);
             res.write("<html><body><h1>404 - Internal Server Error</h1><p>Index.html not found. Pleaser build your server</p></body></html>");
             res.end();
         });
         return;
       } else if (req.url.match(/\/testEntryApi(\?.*)?$/)!=null) {
         req.url=options.testEntryApi;
         console.log("Redirecting to api test...")
         serve(req,res, function(err) {
           res.setHeader('Content-Type', 'text/html');
             res.writeHead(res.statusCode);
             res.write("<html><body><h1>404 - Internal Server Error</h1><p>Index.html not found. Pleaser build your server</p></body></html>");
             res.end();
         });
         return;
       } else if(req.url.match(/\/favicon\.ico$/)!=null) {
         req.url=options.testApi;
         console.log("Loading favicon...")
       } else {
        req.url=options.mainHtml;
        console.log("Serving main...")
       }
        req.method="GET";
        console.log("Serving: "+req.url)
        fs.readFile(options.mainDir+req.url, 'utf8', function (err,data) {
          res.setHeader('Content-Type', 'text/html');
          if(err) {
            res.writeHead(res.statusCode);
            res.write("<html><body><h1>404 - Internal Server Error</h1><p>Index.html not found. Pleaser build your server</p></body></html>");
            res.end();
            return;
          }
          //console.log(data);
          var pathname = url.parse(oldPath).pathname;
          var id=pathname.split("/")[1] || "X"
          var result=data.replace(/\$CLIENT_ID/g, id);
          res.writeHead(200);
          res.write(result);
          res.end();
          return;
        });
      }
    })
  } catch(e) {
      res.statusCode=500;
        res.setHeader('Content-Type', 'text/html');
        res.writeHead(res.statusCode);
        res.write("<html><body><h1>500 - Internal Server Error</h1><p>"+e.toString()+"</p></body></html>");
        res.end();
  }
};
var proxyServer = http.createServer(handler);
if(argv.https) {
  var privateKey = fs.readFileSync('node_stuff/https/private.pem').toString();
  var certificate = fs.readFileSync('node_stuff/https/cert.cer').toString();
  var credentials = {key: privateKey, cert: certificate,passphrase:"testPass"};
  proxyServer = https.createServer(credentials,handler);
}
proxyServer.listen(options.port)
