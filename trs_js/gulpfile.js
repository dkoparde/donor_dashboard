var gulp = require('gulp')
var sass = require('gulp-sass')
var uglify = require('gulp-uglify')
var jshint = require('gulp-jshint');
var rename = require("gulp-rename");
var rjsOptimize = require("gulp-requirejs-optimize")
var del = require('del');
var sourcemaps = require("gulp-sourcemaps")
var stylish = require('jshint-stylish');
var bowerFiles = require("main-bower-files");
var bowerSrc = require("gulp-bower-src");
var gulpFilter = require("gulp-filter")
var bower = require("gulp-bower")
var flatten = require("gulp-flatten")
var es = require("event-stream")
var rjs = require("requirejs");
var pack = require("./package.json")
var glob = require("glob");
var jsdoc= require("gulp-jsdoc3")
var flatten = require('gulp-flatten');
var Server = require('karma').Server;
var babel = require("gulp-babel");
var concat = require("gulp-concat")
var gulpif=require("gulp-if");
var argv = require("yargs").argv
var util=require("gulp-util");
var webpack=require("gulp-webpack");
var zip=require("gulp-zip");

var outDir = argv.outDir || "build";
console.log("Output directory: "+outDir)
var projectInfo = {
  name: pack.name,
  version: pack.version,
  description: pack.description
}
var isProduction=argv.prod || argv.production;
var config = {
  buildSourceMaps: !isProduction,
  minification: !!(isProduction || argv.staging)?true:false,
  scriptsDir: "src/scripts",
  assetsDir: "src/assets",
  jsDocOutput: outDir+"/api/doc",
  //yuiDocOutput: "./doc/js/yui",
  cssDir: outDir + "/styles/lib",
  jsDir: outDir + "/scripts",
  jsLibDir: outDir + "/scripts/lib",
  cssMinDir: outDir + "/styles/min",
  jsMinDir: outDir + "/scripts/lib/min",
  fontDir: outDir + "/fonts",
  sourceMaps: outDir + "/sourceMaps",
  compiledSassDir: outDir + "/styles",
  hostedSrc: outDir + "/src",
  sassSrc: "./src/sass",
  htmlSrc: "./src/pages",
  sassExternalDir: "./src/sass/external",
  apiSrc: "./src/scripts/api/**/*.js",
  apiOut: argv.apiOut || outDir+"/api",
  mainOut: argv.mainOut || outDir+"/main",
  webpackConfig:"./webpack.config.js",
  apiZipName: argv.apiZipName || "OvationtixAPI.zip",
  mainZipName: argv.mainZipName || "Ovationtix_CI.zip",
  zipOut: argv.zipOut || outDir+"/zip"

}

function watchHandle(err) {
    console.log(err.toString());
    this.emit('end');
}

gulp.task('clean', function() {
  del([outDir])
});
gulp.task('clean-node-modules', function() {
  del(["node_modules"])
});

gulp.task('jshint', function() {
  return gulp.src(config.scriptsDir + "/**/*.js")
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

function sassTask() {
  var sassConfig=config.minification?{outputStyle: "compressed"}:{};
  return gulp.src(config.sassSrc + "/**/*.scss")
    .pipe(gulpif(config.buildSourceMaps, sourcemaps.init()))
    .pipe(sass(sassConfig))
    .on('error',watchHandle)
    .pipe(gulpif(config.buildSourceMaps, sourcemaps.write()))
    .on('error',watchHandle)
    .pipe(gulp.dest(config.compiledSassDir))
}

gulp.task('sass-dev', function() {
  return sassTask();
});

gulp.task('sass', ['bower'], function() {
  return sassTask();
});


// grab other fonts from assets directory and push into public
gulp.task('fonts', function() {
  return gulp.src(config.assetsDir + '/**/*.{eot,svg,ttf,woff,woff2}')
         .pipe(gulp.dest(config.fontDir));
});
gulp.task('bower:install', function() {
  return bower();
});
gulp.task("bower:setup", ["bower:install"], function() {
  var jsFilter = gulpFilter('**/*.js', {
    restore: true
  });
  var cssFilter = gulpFilter('*.css', {
    restore: true
  });
  var fontFilter = gulpFilter(['**/*.eot', '**/*.woff', '**/*.woff2', '**/*.svg', '**/*.ttf'], {
    restore: true
  });
  var sassFilter = gulpFilter(['**/_*.scss', '**/_*.sass']);
  var s1 = gulp.src(bowerFiles())

  // grab vendor js files from bower_components, minify and push in /public
  .pipe(jsFilter)
    .pipe(gulpif(config.minification, uglify()))
    .pipe(gulp.dest(config.jsLibDir))
    .pipe(jsFilter.restore)

  // grab vendor css files from bower_components, minify and push in /public
  .pipe(cssFilter)
    .pipe(gulp.dest(config.cssDir))
    .pipe(cssFilter.restore)

  // grab vendor font files from bower_components and push in /public
  var s2 = bowerSrc()
    .pipe(fontFilter)
    .pipe(flatten())
    .pipe(gulp.dest(config.fontDir))
    .pipe(fontFilter.restore)

    .pipe(sassFilter)
    .pipe(gulp.dest(config.sassExternalDir));

  return es.concat(s1, s2);

});

gulp.task('uglify', function() {
  return gulp.src(config.scriptsSrc + "/**/*.js")
    .pipe(uglify())
    .pipe(gulp.dest(config.scriptsOut))
});
gulp.task("webpack", function() {
  var webpackOptions=require(config.webpackConfig);
  return webpack(webpackOptions).pipe(gulp.dest(config.mainOut));
})

gulp.task("zip-webpack",["webpack"], function() {
  return gulp.src(config.mainOut+"/**/*")
    .pipe(zip(config.mainZipName))
    .pipe(gulp.dest(config.zipOut))
})

gulp.task("api-build", function() {
  var webpackOptions=require(config.webpackConfig);
  webpackOptions.entry={
    api: './src/scripts/api/apiDemo.js'
  };
  var stream= gulp.src(config.apiSrc).pipe(webpack(webpackOptions))
    return stream.pipe(gulp.dest(config.apiOut));
})
gulp.task('jsdoc', function (cb) {
    gulp.src([config.apiSrc], {read: false})
        .pipe(jsdoc(cb))
});
gulp.task('doc-api',['jsdoc'], function (cb) {
    return gulp.src("./docs/gen/**/*")
        .pipe(gulp.dest(config.jsDocOutput))
});

gulp.task("build-api", ["doc-api","api-build"], function() {
  return gulp.src(config.jsDocOutput+"/**/*")
    .pipe(gulp.dest(config.apiOut+"/doc"))
});
gulp.task("zip-api", ["build-api"], function() {
  return gulp.src(config.apiOut+"/**/*")
    .pipe(zip(config.apiZipName))
    .pipe(gulp.dest(config.zipOut))
});
/**
 * Run test once and exit
 */
gulp.task('test', function(done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, done).start();
});

/**
 * Watch for file changes and re-run tests on each change
 */
gulp.task('test-debug', function(done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: false
  }, done).start();
});

/**
 * Watch for file changes and re-run tests on each change
 */
gulp.task('test-watch', function(done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: false,
    autoWatch: true
  }, done).start();
});

function buildSrc(source,dest,hostedDest,sourceRoot) {
  return gulp.src(source)
  .pipe(gulpif(config.buildSourceMaps,gulp.dest(hostedDest)))
    .pipe(gulpif(config.buildSourceMaps, sourcemaps.init()))
    .pipe(babel({
      "modules": "amd"
    }))
    .on('error',watchHandle)
    .pipe(gulpif(config.minification, uglify()))
    //  .pipe(concat("all.js"))
    .pipe(gulpif(config.buildSourceMaps, sourcemaps.write({includeContent: false, sourceRoot: sourceRoot || "/src"})))
    .on('error',watchHandle)

    .pipe(gulp.dest(dest))
}

gulp.task("babel", function() {
  var configSrc=null;
  if(argv.production) {
    configSrc="config/prod/**/*.js"
  } else if(argv.staging){
    configSrc="config/staging/**/*.js"
  } else {
    configSrc="config/dev/**/*.js";
  }
  util.log("Using log file: "+configSrc);
  var s1=buildSrc(["src/scripts/!(uncompiled)/**/*.js","src/scripts/*.js"],config.jsDir,outDir+"/src");
  var s2=buildSrc([configSrc],config.jsDir+"/config",outDir+"/src/config","/src/config");

  return es.concat(s1,s2);
});

gulp.task("copy-uncompiled",function() {
  var s1 = gulp.src("src/scripts/uncompiled/**/*")
    .pipe(gulp.dest(config.jsDir));
});
gulp.task("copy-markup",function() {
  var s1 = gulp.src(config.htmlSrc+"/**/*")
    .pipe(gulp.dest(outDir));
});

gulp.task('watch', function() {
  gulp.watch("./src/sass/**/*.scss", ["sass-dev"]);
  gulp.watch("./src/scripts/**/*.js", ["build-scripts"]);
  gulp.watch("./src/assets/**/*", ["fonts"]),
  gulp.watch("./config/dev/**/*.js", ["build-scripts"]);
  gulp.watch("./src/pages/**/*", ["copy-markup"]);
  gulp.watch("bower.json", ["bower"]);
  gulp.watch("gulpfile.js", ["bower","build-scripts"]);
})


gulp.task("build-scripts", ["babel","copy-uncompiled"], function() {
  var s1 = gulp.src("src/scripts/app.js")
    .pipe(gulp.dest(config.jsDir))
    .on('error',watchHandle);
  return s1;
});



gulp.task("bower", ["bower:install", "bower:setup"])
gulp.task("build", ["bower:setup", 'sass','build-scripts','copy-markup','fonts'])
gulp.task("clean-build", ['clean', "build"]);
gulp.task("clean-build-webpack", ['clean', "zip-api", "zip-webpack"]);
gulp.task("version", function() {
    console.log(pack.version);
  })
  //gulp.task("default", ['uglify','sass-min'])
gulp.task("default", ['uglify'])
gulp.task("staging", ['bower', 'sass'])
gulp.task("staging-webpack", ['sass','copy-uncompiled','webpack','doc-api'])
